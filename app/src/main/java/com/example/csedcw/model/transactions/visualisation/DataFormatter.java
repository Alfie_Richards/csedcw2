package com.example.csedcw.model.transactions.visualisation;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.ValueFormatter;

import java.text.DecimalFormat;

public class DataFormatter extends ValueFormatter {

    private DecimalFormat format = new DecimalFormat("£0.00");

    @Override
    public String getBarLabel(BarEntry barEntry) {
        return barEntry.getY() != 0 ? format.format(barEntry.getY()) : "";
    }

    @Override
    public String getAxisLabel(float value, AxisBase axis) {
        return format.format(value);
    }
}
