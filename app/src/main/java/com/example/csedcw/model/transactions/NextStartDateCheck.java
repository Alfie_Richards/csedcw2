package com.example.csedcw.model.transactions;

        import java.time.LocalDate;

public interface NextStartDateCheck {
    boolean test(LocalDate date);
}
