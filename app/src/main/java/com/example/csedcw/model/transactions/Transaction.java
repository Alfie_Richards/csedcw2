package com.example.csedcw.model.transactions;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.squareup.moshi.Json;

import org.jetbrains.annotations.NotNull;

import java.time.LocalDateTime;
import java.time.Period;
import java.time.ZonedDateTime;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAmount;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Entity(tableName = "transaction")
public class Transaction {
    @PrimaryKey
    @NonNull
    private final String id;
    private final String currency;
    private final int amount;

    private final int recurrenceIndex;

    @Json(name = "notes")
    private final String category;
    private String created;
    private transient ZonedDateTime utcDateTimeCreated;

    public Transaction(@NotNull String id, String currency, String category, int amount, String created, int recurrenceIndex) {
        this.id = id;
        this.currency = currency;
        this.amount = amount;
        this.category = category;
        this.created = created;
        this.recurrenceIndex = recurrenceIndex;
    }

    public Transaction(@NotNull String id, String currency, int amount, String notes, String created) {
        this.id = id;
        this.currency = currency;
        this.amount = amount;
        this.created = created;
        this.category = notes;
        this.recurrenceIndex = 0;
    }

    public ZonedDateTime getDateTimeCreated() {
        if (utcDateTimeCreated == null) {
            if (!created.isEmpty()) {
                try {
                    utcDateTimeCreated = ZonedDateTime.parse(created);
                } catch (DateTimeParseException ex) {
                    created = "";
                    utcDateTimeCreated = ZonedDateTime.now();
                }
            } else {
                utcDateTimeCreated = ZonedDateTime.now();
            }
        }

        return utcDateTimeCreated;
    }

    public String getCreated() {
        return created;
    }

    public int getAmount() { return amount; }

    public int getAmountSpent() {
        return amount < 0 ? -amount : 0;
    }

    public String getId() {
        return id;
    }

    public String getCategory() {
        return category;
    }

    public String getCurrency() {
        return currency;
    }

    public int getRecurrenceIndex() {
        return recurrenceIndex;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id='" + id + '\'' +
                ", currency='" + currency + '\'' +
                ", amount=" + amount +
                ", recurrenceIndex=" + recurrenceIndex +
                ", category='" + category + '\'' +
                ", created='" + created + '\'' +
                ", utcDateTimeCreated=" + utcDateTimeCreated +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transaction that = (Transaction) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public enum Recurrence {
        NEVER(Period.ofDays(0)),
        DAILY(Period.ofDays(1)),
        WEEKLY(Period.ofWeeks(1)),
        MONTHLY(Period.ofMonths(1)),
        YEARLY(Period.ofYears(1));

        private TemporalAmount temporalAmount;

        Recurrence(TemporalAmount temporalAmount) {
            this.temporalAmount = temporalAmount;
        }

        public TemporalAmount getTemporalAmount() {
            return temporalAmount;
        }
    }
}
