package com.example.csedcw.model.transactions.visualisation;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.ValueFormatter;

import java.text.DateFormatSymbols;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class DayXAxisFormatter extends ValueFormatter {
    private static final String[] days = new String[] {"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"};

    @Override
    public String getAxisLabel(float value, AxisBase axis) {
        if ((int)value != value) return "";

        int index = (int)value - 1;
        if (!(0 <= index && index < 7)) {
            return String.valueOf(value);
        }

        return days[index];
    }
}
