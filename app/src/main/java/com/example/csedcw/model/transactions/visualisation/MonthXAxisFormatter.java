package com.example.csedcw.model.transactions.visualisation;

import android.renderscript.Sampler;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.ValueFormatter;

import java.text.DateFormatSymbols;

public class MonthXAxisFormatter extends ValueFormatter {
    @Override
    public String getAxisLabel(float value, AxisBase axis) {
        if ((int)value != value) return "";

        return String.valueOf((int) value);
    }
}
