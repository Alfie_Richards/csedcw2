package com.example.csedcw.model.transactions.visualisation;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.ValueFormatter;

import java.text.DateFormat;
import java.text.DateFormatSymbols;

public class YearXAxisFormatter extends ValueFormatter {
    private static final String[] months = DateFormatSymbols.getInstance().getShortMonths();

    @Override
    public String getAxisLabel(float value, AxisBase axis) {
        if ((int)value != value) return "";

        int index = (int)value - 1;
        if (!(0 <= index && index < 12)) {
            return String.valueOf(value);
        }

        return months[index];
    }
}
