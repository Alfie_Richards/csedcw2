package com.example.csedcw.model.transactions;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TimePeriod {
    private String name;

    private LocalDate startDate;
    private LocalDate endDate;

    private NextStartDateCheck nextStartDateCheck;
    private int numPerYear;

    public TimePeriod(String name, NextStartDateCheck nextStartDateCheck, int numPerYear) {
        this.name = name;
        this.nextStartDateCheck = nextStartDateCheck;
        this.startDate = lastValid(LocalDate.now());
        this.endDate = nextValid(LocalDate.now());
        this.numPerYear = numPerYear;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public double getProgress() {
        return 0;
    }

    public String getName() {
        return name;
    }

    private LocalDate nextValid(LocalDate start) {
        while (!nextStartDateCheck.test(start.plusDays(1))) {
            start = start.plusDays(1);
        }
        return start;
    }

    private LocalDate lastValid(LocalDate start) {
        while (!nextStartDateCheck.test(start)) {
            start = start.minusDays(1);
        }
        return start;
    }

    public int getNumPerYear() {
        return numPerYear;
    }

    @Override
    public String toString() {
        return "TimePeriod{" +
                "startDate=" + startDate +
                ", endDate=" + endDate +
                ", name='" + name + '\'' +
                '}';
    }
}
