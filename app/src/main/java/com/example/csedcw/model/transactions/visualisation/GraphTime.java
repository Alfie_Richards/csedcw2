package com.example.csedcw.model.transactions.visualisation;

import com.example.csedcw.model.transactions.Transaction;
import com.github.mikephil.charting.formatter.ValueFormatter;

import java.time.ZonedDateTime;
import java.time.temporal.IsoFields;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public enum GraphTime {
    WEEK(transactions -> transactions.stream()
            .filter(tr -> tr.getDateTimeCreated().get(IsoFields.WEEK_OF_WEEK_BASED_YEAR) == ZonedDateTime.now().get(IsoFields.WEEK_OF_WEEK_BASED_YEAR))
            .collect(Collectors.groupingBy(tr -> tr.getDateTimeCreated().getDayOfWeek().getValue(),
                    Collectors.summingDouble(tr -> {
                        int amountSpent = tr.getAmountSpent();

                        return (double) amountSpent / 100;
                    }))), new DayXAxisFormatter(), 7),
    MONTH(transactions -> transactions.stream()
            .filter(tr -> tr.getDateTimeCreated().getMonthValue() == ZonedDateTime.now().getMonthValue())
            .collect(Collectors.groupingBy(tr -> tr.getDateTimeCreated().getDayOfMonth(),
                    Collectors.summingDouble(tr -> {
                        int amountSpent = tr.getAmountSpent();

                        return (double) amountSpent / 100;
                    }))), new MonthXAxisFormatter(), 31),
    YEAR(transactions -> transactions.stream()
            .filter(tr -> tr.getDateTimeCreated().getYear() == ZonedDateTime.now().getYear())
            .collect(Collectors.groupingBy(tr -> tr.getDateTimeCreated().getMonthValue(),
                    Collectors.summingDouble(tr -> {
                        int amountSpent = tr.getAmountSpent();

                        return (double) amountSpent / 100;
                    }))), new YearXAxisFormatter(), 12);

    public Function<List<Transaction>, Map<Integer, Double>> dataFunction;
    public ValueFormatter xAxisFormatter;
    public int maxMagnitude;

    GraphTime(Function<List<Transaction>, Map<Integer, Double>> dataFunction, ValueFormatter xAxisFormatter, int maxMagnitude) {
        this.dataFunction = dataFunction;
        this.xAxisFormatter = xAxisFormatter;
        this.maxMagnitude = maxMagnitude;
    }
}