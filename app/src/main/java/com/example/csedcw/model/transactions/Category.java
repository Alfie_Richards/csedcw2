package com.example.csedcw.model.transactions;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Objects;

import kotlin.jvm.Transient;

@Entity(tableName = "category")
public class Category {

    @PrimaryKey
    @NonNull
    private String name;

    private String timePeriodName;

    private int totalBudgetPence;

    public Category(@NotNull String name, String timePeriodName, int totalBudgetPence) {
        this.name = name;
        this.timePeriodName = timePeriodName;
        this.totalBudgetPence = totalBudgetPence;
    }

    public String getName() {
        return name;
    }

    public int getTotalBudgetPence() {
        return totalBudgetPence;
    }

    public String getTimePeriodName() {
        return timePeriodName;
    }

    public void setBudget(int amount) {
        totalBudgetPence = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Category category = (Category) o;
        return name.equals(category.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "Category{" +
                "name='" + name + '\'' +
                ", timePeriod=" + timePeriodName +
                ", budget=" + totalBudgetPence +
                '}';
    }
}
