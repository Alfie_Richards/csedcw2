package com.example.csedcw.sorting;

import java.util.Comparator;
import java.util.List;

/**
 * A class providing methods for sorting a list with the Quicksort algorithm, using
 * the comparison relation given by the implemented {@code Comparable} base.
 *
 * @author Joshua Thelwall
 * @see Comparable
 */
public class QuickSorter<T> {

    private Comparator<T> comparator;

    public QuickSorter(Comparator<T> comparator) {
        this.comparator = comparator;
    }

    public Comparator<T> getComparator() {
        return comparator;
    }

    public void setComparator(Comparator<T> comparator) {
        this.comparator = comparator;
    }


    /**
     * Sorts the items in the passed index range within the given list in place, using the Quicksort algorithm.
     * Implementation is based on description found at https://en.wikipedia.org/wiki/Quicksort, using the following optimisations/implementation details:
     * <ul>
     *     <li>Uses the Hoare partition scheme</li>
     *     <li>Uses a pivot at the centre of the list</li>
     *     <li>Uses the median-of-three pivot choice</li>
     * </ul>
     *
     * @param items     The list of items to sort
     * @param lowIndex  The first index of the items to sort within the list
     * @param highIndex The last index of the items to sort within the list
     */
    public void quicksort(List<T> items, int lowIndex, int highIndex) {
        if (lowIndex < highIndex) {
            pivotMedianOfThree(items, lowIndex, highIndex);

            int pivot = partition(items, lowIndex, highIndex);
            quicksort(items, lowIndex, pivot);
            quicksort(items, pivot + 1, highIndex);
        }
    }

    /**
     * Partitions the list at the centre pivot, moving all the elements less than the pivot below it and greater than above it.
     * Uses the Hoare partition scheme, in which pairs of elements (a, b) with indices i_a < i_b but values a >= pivot and b <= pivot
     * are swapped from the outside inward.
     *
     * @param items     The list of items being sorted
     * @param lowIndex  The first index of the items to sort within the list
     * @param highIndex The last index of the items to sort within the list
     * @return The index of the pivot after partitioning
     */
    private int partition(List<T> items, int lowIndex, int highIndex) {
        T pivot = items.get((highIndex + lowIndex) / 2);
        int i = lowIndex - 1;
        int j = highIndex + 1;

        boolean partitioning = true;

        while (partitioning) {
            do {
                i++;
            } while (comparator.compare(items.get(i), pivot) < 0);

            do {
                j--;
            } while (comparator.compare(items.get(j), pivot) > 0);

            if (i >= j) {
                partitioning = false;
            } else {
                swap(items, i, j);
            }
        }

        return j;
    }

    /**
     * Swaps the elements at the start, centre, and end of the given range so that
     * the smallest is at the start, the median is at the centre, and the largest is at the end,
     * effectively choosing the median as the pivot, which is at the centre.
     *
     * @param items     The list of items being sorted
     * @param lowIndex  The first index of the items to sort within the list
     * @param highIndex The last index of the items to sort within the list
     */
    private void pivotMedianOfThree(List<T> items, int lowIndex, int highIndex) {
        int midIndex = (highIndex + lowIndex) / 2;

        if (comparator.compare(items.get(midIndex), items.get(lowIndex)) < 0) {
            swap(items, lowIndex, midIndex);
        }

        if (comparator.compare(items.get(highIndex), items.get(lowIndex)) < 0) {
            swap(items, lowIndex, highIndex);
        }

        if (!(comparator.compare(items.get(midIndex), items.get(highIndex)) < 0)) {
            swap(items, midIndex, highIndex);
        }
    }

    /**
     * Swaps the pair of elements at the two given indices within the list.
     *
     * @param items              The list in which to swap the elements
     * @param firstElementIndex  The index of the first element of the pair to swap
     * @param secondElementIndex The index of the second element of the pair to swap
     */
    private void swap(List<T> items, int firstElementIndex, int secondElementIndex) {
        T originalFirstElementValue = items.get(firstElementIndex);

        items.set(firstElementIndex, items.get(secondElementIndex));
        items.set(secondElementIndex, originalFirstElementValue);
    }

}
