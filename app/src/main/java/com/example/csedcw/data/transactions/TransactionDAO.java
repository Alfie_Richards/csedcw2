package com.example.csedcw.data.transactions;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.csedcw.model.transactions.Category;
import com.example.csedcw.model.transactions.Transaction;

import java.util.List;

@Dao
public interface TransactionDAO {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Transaction transaction);

    @Query("SELECT * FROM `transaction`")
    LiveData<List<Transaction>> getTransactions();

    @Query("SELECT * FROM `transaction` WHERE category IN (:categoryNames)")
    LiveData<List<Transaction>> getTransactionsInCategories(List<String> categoryNames);

    @Delete
    void delete(Transaction transaction);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertAll(List<Transaction> transactions);
}
