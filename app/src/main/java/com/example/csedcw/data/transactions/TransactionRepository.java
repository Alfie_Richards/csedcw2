package com.example.csedcw.data.transactions;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.example.csedcw.model.transactions.Category;
import com.example.csedcw.model.transactions.Transaction;

import java.util.List;
import java.util.stream.Collectors;

public class TransactionRepository {

    private TransactionDAO transactionDAO;

    public TransactionRepository(Application application) {
        TransactionRoomDatabase database = TransactionRoomDatabase.getDatabase(application);
        transactionDAO = database.transactionDAO();
    }

    public LiveData<List<Transaction>> getTransactionsData() {
        return transactionDAO.getTransactions();
    }

    public void insertAll(List<Transaction> transactions) {
        TransactionRoomDatabase.databaseWriteExecutor.execute(() -> {
            transactionDAO.insertAll(transactions);
        });
    }

    public void insert(Transaction transaction) {
        TransactionRoomDatabase.databaseWriteExecutor.execute(() -> {
            transactionDAO.insert(transaction);
        });
    }

    public void delete(Transaction transaction) {
        TransactionRoomDatabase.databaseWriteExecutor.execute(() -> {
            transactionDAO.delete(transaction);
        });
    }

    public LiveData<List<Transaction>> getTransactionsInCategoriesData(List<Category> categories) {
        return transactionDAO.getTransactionsInCategories(categories.stream().map(Category::getName).collect(Collectors.toList()));
    }
}
