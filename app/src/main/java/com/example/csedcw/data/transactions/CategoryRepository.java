package com.example.csedcw.data.transactions;


import android.app.Application;

import androidx.lifecycle.LiveData;

import com.example.csedcw.model.transactions.Category;

import java.util.List;

public class CategoryRepository {
    private CategoryDAO categoryDAO;
    private LiveData<List<Category>> categoriesData;

    public CategoryRepository(Application application) {
        TransactionRoomDatabase database = TransactionRoomDatabase.getDatabase(application);
        categoryDAO = database.categoryDAO();
        categoriesData = categoryDAO.getCategories();
    }

    public LiveData<List<Category>> getCategories() {
        return categoriesData;
    }

    public void insert(Category category) {
        TransactionRoomDatabase.databaseWriteExecutor.execute(() -> {
            categoryDAO.insert(category);
        });
    }

    public void remove(Category category) {
        TransactionRoomDatabase.databaseWriteExecutor.execute(() -> {
            categoryDAO.delete(category);
        });
    }
}
