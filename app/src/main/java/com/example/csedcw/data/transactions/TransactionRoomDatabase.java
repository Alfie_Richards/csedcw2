package com.example.csedcw.data.transactions;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.csedcw.model.transactions.Category;
import com.example.csedcw.model.transactions.Transaction;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {Transaction.class, Category.class}, version = 1)
public abstract class TransactionRoomDatabase extends RoomDatabase {
    public abstract TransactionDAO transactionDAO();
    public abstract CategoryDAO categoryDAO();

    private static volatile TransactionRoomDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 4;
    static final ExecutorService databaseWriteExecutor = Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    static TransactionRoomDatabase getDatabase(Context context) {
        if (INSTANCE == null) {
            synchronized (TransactionRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(), TransactionRoomDatabase.class, "transaction").build();
                }
            }
        }

        return INSTANCE;
    }
}
