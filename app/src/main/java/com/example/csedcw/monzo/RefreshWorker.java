package com.example.csedcw.monzo;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.example.csedcw.monzo.response.AccessResponse;

public class RefreshWorker extends Worker {

    private final MonzoAuthenticator authenticator;

    public RefreshWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);

        this.authenticator = new MonzoAuthenticator();
    }

    @NonNull
    @Override
    public Result doWork() {
        AccessResponse response = authenticator.refreshAccess(getInputData().getString("REFRESH_TOKEN"));
        if (response == null) {
            return Result.retry();
        }

        Log.d("Budget", "New Access " + response.getAccessToken());
        Log.d("Budget", "New Refresh " + response.getRefreshToken());
        Log.d("Budget", "New Expires " + response.getExpiresInSeconds());

        return Result.success(new Data.Builder()
                .putString("ACCESS_TOKEN", response.getAccessToken())
                .putString("REFRESH_TOKEN", response.getRefreshToken())
                .putInt("EXPIRES_IN", response.getExpiresInSeconds()).build());
    }
}
