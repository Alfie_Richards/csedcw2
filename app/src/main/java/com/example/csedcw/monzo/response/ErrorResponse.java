package com.example.csedcw.monzo.response;

public class ErrorResponse {
    private final String code;
    private final String error;

    public ErrorResponse(String code, String error) {
        this.code = code;
        this.error = error;
    }

    public String getCode() {
        return code;
    }

    public String getError() {
        return error;
    }
}
