package com.example.csedcw.monzo.response;

public class PingResponse {
    private final boolean authenticated;

    public PingResponse(boolean authenticated) {
        this.authenticated = authenticated;
    }

    public boolean isAuthenticated() {
        return authenticated;
    }
}
