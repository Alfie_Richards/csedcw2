package com.example.csedcw.monzo.response;

import java.util.List;

public class AccountsResponse {
    private final List<Account> accounts;

    public AccountsResponse(List<Account> accounts) {
        this.accounts = accounts;
    }

    public List<Account> getAccounts() {
        return accounts;
    }
}
