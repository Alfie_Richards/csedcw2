package com.example.csedcw.monzo.response;

import java.util.List;

import com.example.csedcw.model.transactions.Transaction;

public class TransactionsResponse {
    private final List<Transaction> transactions;

    public TransactionsResponse(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

}
