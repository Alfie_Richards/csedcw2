package com.example.csedcw.monzo;

import android.util.Log;

import androidx.core.util.Consumer;

import com.example.csedcw.monzo.response.ErrorResponse;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import com.example.csedcw.Constants;
import com.example.csedcw.monzo.response.AccessResponse;
import com.example.csedcw.monzo.response.PingResponse;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

import static com.example.csedcw.Constants.LOG_TAG;

public class MonzoAuthenticator {

    private final OkHttpClient client;
    private final Moshi moshi = new Moshi.Builder().build();

    private final JsonAdapter<PingResponse> pingResponseAdapter = moshi.adapter(PingResponse.class);
    private final JsonAdapter<AccessResponse> accessResponseAdapter = moshi.adapter(AccessResponse.class);
    private final JsonAdapter<ErrorResponse> errorResponseAdapter = moshi.adapter(ErrorResponse.class);

    public MonzoAuthenticator() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        this.client = new OkHttpClient.Builder().addInterceptor(logging).build();
    }

    public AccessResponse refreshAccess(String refreshToken) {
        Request req = new Request.Builder()
                .url("https://api.monzo.com/oauth2/token")
                .post(new FormBody.Builder()
                        .add("grant_type", "refresh_token")
                        .add("client_id", Constants.CLIENT_ID)
                        .add("client_secret", Constants.CLIENT_SECRET)
                        .add("refresh_token", refreshToken).build())
                .build();

        Log.d(LOG_TAG, "Refresh " + refreshToken);

        Log.d(LOG_TAG, req.toString());


        try (Response response = client.newCall(req).execute()) {
            String responseString = response.body().string();

            AccessResponse accessResponse = accessResponseAdapter.fromJson(responseString);

            if (accessResponse == null || accessResponse.getAccessToken() == null) {
                Log.d(LOG_TAG, "Error parsing refresh access response");
                return null;
            }

            return accessResponse;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void retrieveIsAuthenticated(String accessToken, Consumer<Boolean> successCallback, Consumer<IOException> failureCallback) {
        if (accessToken == null || accessToken.isEmpty()) {
            successCallback.accept(false);
            return;
        }

        Request req = new Request.Builder()
                .url("https://api.monzo.com/ping/whoami")
                .header("Authorization", "Bearer " + accessToken)
                .build();


        Log.d(LOG_TAG, "Ping. whoami? " + accessToken);

        client.newCall(req).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                failureCallback.accept(e);
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) {
                if (response.code() == 400) {
                    successCallback.accept(false);
                    return;
                }

                PingResponse pingResponse;


                try {
                    pingResponse = pingResponseAdapter.fromJson(response.body().source());
                } catch (IOException e) {
                    e.printStackTrace();
                    failureCallback.accept(e);

                    return;
                } finally {
                    response.close();
                }

                if (pingResponse == null) {
                    Log.d(LOG_TAG, "Failed to parse ping response.");
                    failureCallback.accept(null);
                    return;
                }

                Log.d(LOG_TAG, "Ping response: " + pingResponse.isAuthenticated());

                successCallback.accept(pingResponse.isAuthenticated());
            }
        });
    }

    public void retrieveAccessResponse(String authCode, Consumer<AccessResponse> successCallback, Consumer<IOException> failureCallback) {
        Request req = new Request.Builder()
                .url(HttpUrl.parse("https://api.monzo.com/oauth2/token"))
                .post(new FormBody.Builder()
                        .add("grant_type", "authorization_code")
                        .add("client_id", Constants.CLIENT_ID)
                        .add("client_secret", Constants.CLIENT_SECRET)
                        .add("redirect_uri", Constants.REDIRECT_URI)
                        .add("code", authCode).build())
                .build();

        Log.d(LOG_TAG, req.toString());

        client.newCall(req).enqueue(new Callback() {
            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) {
                AccessResponse accessResponse;

                try {
                    accessResponse = accessResponseAdapter.fromJson(response.body().source());
                } catch (IOException e) {
                    e.printStackTrace();
                    failureCallback.accept(e);
                    return;
                } finally {
                    response.close();
                }

                if (accessResponse == null || accessResponse.getAccessToken() == null) {
                    Log.d(LOG_TAG, "Error in parsing authentication response");
                    failureCallback.accept(null);
                    return;
                }

                successCallback.accept(accessResponse);
            }

            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                failureCallback.accept(e);
            }
        });
    }
}
