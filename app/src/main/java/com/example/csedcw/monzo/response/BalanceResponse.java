package com.example.csedcw.monzo.response;

public class BalanceResponse {
    private final int balance;
    private final String currency;

    public BalanceResponse(int balance, String currency) {
        this.balance = balance;
        this.currency = currency;
    }

    public int getBalancePence() {
        return balance;
    }

    public String getCurrency() {
        return currency;
    }
}
