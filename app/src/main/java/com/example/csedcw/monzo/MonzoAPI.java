package com.example.csedcw.monzo;

import android.util.Log;

import androidx.core.util.Consumer;

import com.example.csedcw.model.transactions.Transaction;
import com.example.csedcw.monzo.response.Account;
import com.example.csedcw.monzo.response.AccountsResponse;
import com.example.csedcw.monzo.response.BalanceResponse;
import com.example.csedcw.monzo.response.ErrorResponse;
import com.example.csedcw.monzo.response.TransactionsResponse;
import com.example.csedcw.ui.MonzoViewModel;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Duration;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

import static com.example.csedcw.Constants.LOG_TAG;

public class MonzoAPI {
    private final MonzoViewModel monzoViewModel;

    private final OkHttpClient client;
    private final Moshi moshi = new Moshi.Builder().build();

    private final JsonAdapter<BalanceResponse> balanceJsonAdapter = moshi.adapter(BalanceResponse.class);
    private final JsonAdapter<TransactionsResponse> transactionJsonAdapter = moshi.adapter(TransactionsResponse.class);

    private final JsonAdapter<AccountsResponse> accountsJsonAdapter = moshi.adapter(AccountsResponse.class);
    private final JsonAdapter<ErrorResponse> errorResponseAdapter = moshi.adapter(ErrorResponse.class);

    public MonzoAPI(MonzoViewModel monzoViewModel) {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        this.client = new OkHttpClient.Builder().addInterceptor(logging).build();

        this.monzoViewModel = monzoViewModel;
    }

    public void getBalancePence(Consumer<BigDecimal> successCallback, Consumer<IOException> failureCallback) {
        checkAccountID(() -> {
            Request req = new Request.Builder()
                    .url(HttpUrl.parse("https://api.monzo.com/balance").newBuilder().addQueryParameter("account_id", monzoViewModel.getAccountIdData().getValue()).build())
                    .header("Authorization", "Bearer " + monzoViewModel.getAccessTokenData().getValue())
                    .build();

            client.newCall(req).enqueue(new Callback() {
                @Override
                public void onResponse(@NotNull Call call, @NotNull Response response) {
                    BalanceResponse balanceResponse;

                    try {
                        String responseString = response.body().string();

                        balanceResponse = balanceJsonAdapter.fromJson(responseString);

                        if (balanceResponse == null || balanceResponse.getCurrency() == null) {
                            ErrorResponse errorResponse = errorResponseAdapter.fromJson(responseString);
                            if (errorResponse != null && errorResponse.getCode() != null) {
                                if (errorResponse.getCode().equals("unauthorized.bad_access_token")) {
                                    failureCallback.accept(new IOException("Unable to update balance because of authentication. Try re-authenticating."));
                                } else if (errorResponse.getCode().equals("forbidden.insufficient_permissions")) {
                                    failureCallback.accept(new IOException("Unable to update balance because of insufficient permissions. Try going to the Monzo app and allowing this app access."));
                                } else {
                                    failureCallback.accept(new IOException("Unable to update balance. Data shown may be out of date."));
                                }

                                return;
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        failureCallback.accept(e);
                        return;
                    } finally {
                        response.close();
                    }


                    Log.d(LOG_TAG, "New balance: " + balanceResponse.getBalancePence());

                    successCallback.accept(new BigDecimal(balanceResponse.getBalancePence()).setScale(2, RoundingMode.HALF_DOWN));
                }

                @Override
                public void onFailure(@NotNull Call call, @NotNull IOException e) {
                    e.printStackTrace();
                    failureCallback.accept(e);
                }
            });
        }, failureCallback);
    }

    public void getTransactions(Consumer<List<Transaction>> successCallback, Consumer<IOException> failureCallback) {
        checkAccountID(() -> {
            Request req = new Request.Builder()
                    .url(HttpUrl.parse("https://api.monzo.com/transactions")
                            .newBuilder()
                            .addQueryParameter("account_id", monzoViewModel.getAccountIdData().getValue())
                            .addQueryParameter("since", DateTimeFormatter.ISO_INSTANT.format(Instant.now().minus(Duration.ofDays(89)))).build())
                    .header("Authorization", "Bearer " + monzoViewModel.getAccessTokenData().getValue())
                    .build();

            client.newCall(req).enqueue(new Callback() {
                @Override
                public void onResponse(@NotNull Call call, @NotNull Response response) {
                    List<Transaction> transactionsResponse;

                    try {
                        transactionsResponse = transactionJsonAdapter.fromJson(response.body().source()).getTransactions();
                    } catch (IOException e) {
                        e.printStackTrace();
                        failureCallback.accept(e);
                        return;
                    } finally {
                        response.close();
                    }

                    if (transactionsResponse == null) {
                        failureCallback.accept(null);
                        return;
                    }

                    Collections.reverse(transactionsResponse);

                    successCallback.accept(transactionsResponse);
                }

                @Override
                public void onFailure(@NotNull Call call, @NotNull IOException e) {
                    e.printStackTrace();
                    failureCallback.accept(e);
                }
            });
        }, failureCallback);
    }

    private void checkAccountID(Runnable successCallback, Consumer<IOException> failureCallback) {
        if (monzoViewModel.getAccountIdData().getValue() != null && !monzoViewModel.getAccountIdData().getValue().isEmpty()) {
            successCallback.run();
            return;
        }

        Request req = new Request.Builder()
                .url("https://api.monzo.com/accounts")
                .header("Authorization", "Bearer " + monzoViewModel.getAccessTokenData().getValue())
                .build();

        client.newCall(req).enqueue(new Callback() {
            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) {
                AccountsResponse accountsResponse;

                try {
                    accountsResponse = accountsJsonAdapter.fromJson(response.body().source());
                } catch (IOException e) {
                    e.printStackTrace();
                    failureCallback.accept(e);
                    return;
                } finally {
                    response.close();
                }

                if (accountsResponse == null || accountsResponse.getAccounts() == null || accountsResponse.getAccounts().isEmpty()) {
                    failureCallback.accept(null);
                    return;
                }

                monzoViewModel.getAccountIdData().postValue(accountsResponse.getAccounts().get(0).getId());

                successCallback.run();
            }

            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                e.printStackTrace();
                failureCallback.accept(e);
            }
        });
    }
}
