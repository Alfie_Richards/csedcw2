package com.example.csedcw.monzo.response;

public class Account {
    private final String id;
    private final String description;
    private final String created;

    public Account(String id, String description, String created) {
        this.id = id;
        this.description = description;
        this.created = created;
    }

    public String getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public String getCreated() {
        return created;
    }
}
