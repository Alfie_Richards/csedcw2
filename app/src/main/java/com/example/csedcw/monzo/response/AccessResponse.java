package com.example.csedcw.monzo.response;

public class AccessResponse {
    private final String access_token;
    private final String refresh_token;
    private final int expires_in;

    public AccessResponse(String access_token, String refresh_token, String user_id, int expires_in) {
        this.access_token = access_token;
        this.refresh_token = refresh_token;
        this.expires_in = expires_in;
    }

    public String getAccessToken() {
        return access_token;
    }

    public String getRefreshToken() {
        return refresh_token;
    }

    public int getExpiresInSeconds() {
        return expires_in;
    }
}
