package com.example.csedcw;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalField;
import java.util.Calendar;
import java.util.Objects;
import java.util.UUID;

import com.example.csedcw.model.transactions.Transaction;
import com.example.csedcw.monzo.MonzoAuthenticator;
import com.example.csedcw.monzo.RefreshWorker;
import com.example.csedcw.ui.MonzoViewModel;
import com.example.csedcw.ui.transactions.TransactionAddDialog;
import com.example.csedcw.ui.transactions.TransactionsViewModel;

import static com.example.csedcw.Constants.LOG_TAG;
import static com.example.csedcw.Constants.REDIRECT_URI;

public class MainActivity extends AppCompatActivity implements TransactionAddDialog.TransactionAddDialogListener {

    private MonzoViewModel monzoViewModel;

    private TransactionsViewModel transactionsViewModel;

    private MonzoAuthenticator monzoAuthenticator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d(LOG_TAG, "Main created");

        monzoAuthenticator = new MonzoAuthenticator();

        monzoViewModel = new ViewModelProvider(this).get(MonzoViewModel.class);
        transactionsViewModel = new ViewModelProvider(this).get(TransactionsViewModel.class);
        transactionsViewModel.setMonzoViewModel(monzoViewModel);

        setupViewModels();

        setContentView(R.layout.activity_main);

        BottomNavigationView navView = findViewById(R.id.nav_view);

        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home,
                R.id.navigation_transactions,
                R.id.navigation_budget,
                R.id.navigation_balance_sheet
        ).build();

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);

        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);
    }

    private void setupViewModels() {
        monzoViewModel.getAccessTokenData().setValue(getPreferences(Context.MODE_PRIVATE).getString("ACCESS_TOKEN", ""));
        monzoViewModel.getRefreshTokenData().setValue(getPreferences(Context.MODE_PRIVATE).getString("REFRESH_TOKEN", ""));
        monzoViewModel.getAccountIdData().setValue(getPreferences(Context.MODE_PRIVATE).getString("ACCOUNT_ID", ""));
        monzoViewModel.getExpiresInSecondsData().setValue(getPreferences(Context.MODE_PRIVATE).getInt("EXPIRES_IN", -1));

        monzoViewModel.getAccessTokenData().observe(this, newToken -> {
            if (newToken != null && !newToken.isEmpty()) {
                Log.d(LOG_TAG, "Access token " + newToken);
                SharedPreferences.Editor editor = getPreferences(Context.MODE_PRIVATE).edit();
                editor.putString("ACCESS_TOKEN", newToken);
                editor.apply();
            }
        });

        monzoViewModel.getRefreshTokenData().observe(this, newToken -> {
            if (newToken != null && !newToken.isEmpty()) {
                Log.d(LOG_TAG, "Refresh token " + newToken);
                SharedPreferences.Editor editor = getPreferences(Context.MODE_PRIVATE).edit();
                editor.putString("REFRESH_TOKEN", newToken);
                editor.apply();
            }
        });

        monzoViewModel.getAccountIdData().observe(this, newAccountId -> {
            if (newAccountId != null && !newAccountId.isEmpty()) {
                Log.d(LOG_TAG, "Account ID " + newAccountId);
                SharedPreferences.Editor editor = getPreferences(Context.MODE_PRIVATE).edit();
                editor.putString("ACCOUNT_ID", newAccountId);
                editor.apply();
            }
        });

        monzoViewModel.getExpiresInSecondsData().observe(this, newExpiresIn -> {
            if (newExpiresIn != null) {
                Log.d(LOG_TAG, "Expires in " + newExpiresIn);
                SharedPreferences.Editor editor = getPreferences(Context.MODE_PRIVATE).edit();
                editor.putInt("EXPIRES_IN", newExpiresIn);
                editor.apply();

                if (newExpiresIn != -1) {
                    Log.d("Budget", "Starting Refresh in " + newExpiresIn);
                    startRefreshWorker();
                }
            }
        });


    }

    @Override
    protected void onStart() {
        super.onStart();

        Log.d(LOG_TAG, getIntent().toString());
        Log.d(LOG_TAG, "Access:" + monzoViewModel.getAccessTokenData().getValue());
        Log.d(LOG_TAG, String.valueOf(monzoViewModel.getIsAuthenticatingData().getValue()));
        Log.d(LOG_TAG, String.valueOf(monzoViewModel.getIsAuthenticatedData().getValue()));

        handleAuthenticationIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        handleAuthenticationIntent(intent);
    }

    private void handleAuthenticationIntent(Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_VIEW)
                && intent.getData() != null
                && Objects.equals(intent.getData().getHost(), Uri.parse(REDIRECT_URI).getHost())
                && !monzoViewModel.getIsAuthenticatedData().getValue()) {
            String authCode = intent.getData().getQueryParameter("code");
            if (authCode == null) return;

            Log.d(LOG_TAG, "Auth code:" + authCode);

            monzoAuthenticator.retrieveAccessResponse(authCode,
                    authResponse -> {
                        Log.d(LOG_TAG, "Response!");

                        monzoViewModel.getAccessTokenData().postValue(authResponse.getAccessToken());
                        monzoViewModel.getRefreshTokenData().postValue(authResponse.getRefreshToken());
                        monzoViewModel.getExpiresInSecondsData().postValue(authResponse.getExpiresInSeconds());

                        runOnUiThread(() -> Toast.makeText(MainActivity.this, R.string.monzo_success_msg, Toast.LENGTH_LONG).show());

                    },
                    ex -> {
                        String errorMessage = getString(ex == null ? R.string.authentication_error : R.string.network_error);

                        if (ex != null) ex.printStackTrace();

                        runOnUiThread(() -> Toast.makeText(MainActivity.this, errorMessage, Toast.LENGTH_LONG).show());

                        monzoViewModel.getIsAuthenticatingData().postValue(false);
                    });
        }
    }

    private void startRefreshWorker() {
        Data data = new Data.Builder().putString("REFRESH_TOKEN", monzoViewModel.getRefreshTokenData().getValue()).build();
        OneTimeWorkRequest refreshRequest = new OneTimeWorkRequest.Builder(RefreshWorker.class).setInputData(data).setInitialDelay(Duration.ofSeconds(monzoViewModel.getExpiresInSecondsData().getValue())).build();

        WorkManager manager = WorkManager.getInstance(this);

        manager.enqueue(refreshRequest);

        manager.getWorkInfoByIdLiveData(refreshRequest.getId()).observe(this, info -> {
            if (info == null) return;

            if (info.getState() == WorkInfo.State.SUCCEEDED) {
                String ACCESS_TOKEN = info.getOutputData().getString("ACCESS_TOKEN");
                String REFRESH_TOKEN = info.getOutputData().getString("REFRESH_TOKEN");
                int EXPIRES_IN = info.getOutputData().getInt("EXPIRES_IN", -1);

                Log.d(LOG_TAG, "Refresh1");

                monzoViewModel.getAccessTokenData().setValue(ACCESS_TOKEN);
                monzoViewModel.getRefreshTokenData().setValue(REFRESH_TOKEN);
                monzoViewModel.getExpiresInSecondsData().setValue(EXPIRES_IN);

                Log.d(LOG_TAG, "Refresh12");

                SharedPreferences.Editor editor = getPreferences(Context.MODE_PRIVATE).edit();
                editor.putString("ACCESS_TOKEN", ACCESS_TOKEN);
                editor.putString("REFRESH_TOKEN", REFRESH_TOKEN);
                editor.putInt("EXPIRES_IN", EXPIRES_IN);
                editor.apply();

                Log.d(LOG_TAG, "Refresh123");
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Log.d(LOG_TAG, "Main destroyed");
    }

    @Override
    public void onTransactionDialogPositiveClick(DialogFragment dialog, EditText amountEditText, Spinner categoryEditText, Spinner recurringSpinner, DatePicker datePicker, TimePicker timePicker) {
        if (amountEditText.getText().toString().isEmpty()) {
            Toast.makeText(this, "Transaction was not added, as amount wasn't specified.", Toast.LENGTH_SHORT).show();
            return;
        }

        int amount = new BigDecimal(amountEditText.getText().toString()).multiply(new BigDecimal(100)).setScale(2, RoundingMode.HALF_UP).intValue();
        String notes = (String) categoryEditText.getSelectedItem();
        Instant dateTime = LocalDateTime.of(datePicker.getYear(), datePicker.getMonth() + 1, datePicker.getDayOfMonth(), timePicker.getHour(), timePicker.getMinute()).atZone(ZoneId.systemDefault()).toInstant();

        Instant transactionMinutes = dateTime.truncatedTo(ChronoUnit.MINUTES);
        Instant nowMinutes = Instant.now().atZone(ZoneId.systemDefault()).truncatedTo(ChronoUnit.MINUTES).toInstant();

        Log.d(LOG_TAG, "DT: " + transactionMinutes);
        Log.d(LOG_TAG, "IN: "+ nowMinutes);

        if (transactionMinutes.equals(nowMinutes)) {
            Log.d(LOG_TAG, "Same minute!");
            dateTime = LocalDateTime.of(datePicker.getYear(), datePicker.getMonth() + 1, datePicker.getDayOfMonth(), timePicker.getHour(), timePicker.getMinute(), LocalDateTime.now().getSecond()).toInstant(ZoneOffset.UTC);
        }

        String created = DateTimeFormatter.ISO_INSTANT.format(dateTime);

        transactionsViewModel.addLocalTransaction(new Transaction(UUID.randomUUID().toString(), "GBP", notes, amount, created, recurringSpinner.getSelectedItemPosition()));
    }

    @Override
    public void onTransactionDialogNegativeClick(DialogFragment dialog) {
    }
}