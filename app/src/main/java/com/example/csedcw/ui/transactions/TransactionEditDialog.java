package com.example.csedcw.ui.transactions;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.csedcw.R;
import com.example.csedcw.model.transactions.Category;
import com.example.csedcw.ui.budget.BudgetViewModel;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class TransactionEditDialog extends DialogFragment {

    public interface TransactionAddDialogListener {
        void onTransactionDialogPositiveClick(DialogFragment dialog, EditText amountEditText, Spinner categoryEditText, Spinner recurringSpinner, DatePicker datePicker, TimePicker timePicker, boolean isExpense);

        void onTransactionDialogNegativeClick(DialogFragment dialog);
    }

    private TransactionAddDialogListener listener;

    List<String> categories;
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        try {
            listener = (TransactionAddDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(requireActivity().toString() + "must implement TransactionAddDialogListener");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = requireActivity().getLayoutInflater().inflate(R.layout.transaction_add_dialog, null);
        builder.setView(view);

        EditText amountEditText = view.findViewById(R.id.amount_edit_text);
        Spinner categorySpinner = view.findViewById(R.id.category_spinner);

        categories = Collections.synchronizedList((new ViewModelProvider(requireActivity()).get(BudgetViewModel.class)).getCategoriesData().getValue().stream().map(Category::getName).collect(Collectors.toList()));

        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter(getContext(), R.layout.support_simple_spinner_dropdown_item, categories);
        categorySpinner.setAdapter(spinnerAdapter);

        (new ViewModelProvider(requireActivity()).get(BudgetViewModel.class)).getCategoriesData().observe(this.getActivity(), list -> {
            list.forEach(category -> {
                if(categories.stream().noneMatch(name -> name.equals(category.getName()))) {
                    categories.add(category.getName());
                    Log.d("Transaction add dialog", "Added category: " + category.getName());
                } else {
                    Log.d("Transaction add dialog", "NOT Added categories");
                }
            });
            spinnerAdapter.notifyDataSetChanged();
            Log.d("Transaction add dialog", "Notified update");
        });

        RadioGroup radioGroup = view.findViewById(R.id.income_expense_button_group);
        RadioButton expense_radio = view.findViewById(R.id.expense_radio_button);
        RadioButton income_radio = view.findViewById(R.id.income_radio_button);

        DatePicker datePicker = view.findViewById(R.id.transaction_date_picker);
        TimePicker timePicker = view.findViewById(R.id.transaction_time_picker);

        Spinner recurringSpinner = view.findViewById(R.id.recurring_spinner);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this.requireContext(), R.array.recurring_items, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        recurringSpinner.setAdapter(adapter);

        radioGroup.check(R.id.expense_radio_button);

        builder.setPositiveButton("Confirm", (dialog, id) -> listener.onTransactionDialogPositiveClick(TransactionEditDialog.this, amountEditText, categorySpinner, recurringSpinner, datePicker, timePicker, expense_radio.isChecked()));
        builder.setNegativeButton("Cancel", (dialog, id) -> listener.onTransactionDialogNegativeClick(TransactionEditDialog.this));

        return builder.create();
    }

}
