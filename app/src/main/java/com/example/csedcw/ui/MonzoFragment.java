package com.example.csedcw.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.csedcw.Constants;
import com.example.csedcw.monzo.MonzoAPI;
import com.example.csedcw.monzo.MonzoAuthenticator;
import com.example.csedcw.ui.transactions.TransactionsViewModel;

import static com.example.csedcw.Constants.LOG_TAG;

public abstract class MonzoFragment extends Fragment {

    protected MonzoAuthenticator monzoAuthenticator;

    protected MonzoViewModel monzoViewModel;

    protected TransactionsViewModel transactionsViewModel;

    protected MonzoAPI monzoAPI;

    protected boolean monzoConnected = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        monzoAuthenticator = new MonzoAuthenticator();

        monzoViewModel = new ViewModelProvider(requireActivity()).get(MonzoViewModel.class);

        transactionsViewModel = new ViewModelProvider(requireActivity()).get(TransactionsViewModel.class);

        monzoAPI = new MonzoAPI(monzoViewModel);

        if (monzoViewModel.getIsAuthenticatedData().getValue()) {
            monzoAuthenticator.retrieveIsAuthenticated(monzoViewModel.getAccessTokenData().getValue(),
                    isAuthenticated -> {
                        if (!isAuthenticated) {
                            Activity activity = getActivity();
                            if (activity != null) {
                                activity.runOnUiThread(() -> {
                                    Toast.makeText(activity, "Monzo account needs to be re-authenticated.", Toast.LENGTH_LONG).show();
                                });
                            }

                            monzoViewModel.getIsAuthenticatedData().postValue(false);
                            startAuthenticationIntent();
                        } else {
                            Activity activity = getActivity();
                            if (activity != null) {
                                SharedPreferences.Editor editor = activity.getPreferences(Context.MODE_PRIVATE).edit();
                                editor.putBoolean("MONZO_CONNECTED", true);
                                editor.apply();
                            }
                        }
                    },
                    ex -> {
                        ex.printStackTrace();
                        Log.d(LOG_TAG, "Problem checking if authenticated");
                        Activity activity = getActivity();
                        if (activity != null) {
                            activity.runOnUiThread(() -> {
                                Toast.makeText(activity, "Unable to check if Monzo account is authenticated. Data shown may be old. Try again later.", Toast.LENGTH_LONG).show();
                            });
                        }
                    });
        }

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();

        monzoConnected = requireActivity().getPreferences(Context.MODE_PRIVATE).getBoolean("MONZO_CONNECTED", false);
        updateTransactions();
    }

    protected void updateTransactions() {
        transactionsViewModel.updateRemoteTransactions(ex -> {
            Activity activity = getActivity();
            if (activity != null) {
                activity.runOnUiThread(() -> Toast.makeText(activity, "Unable to update transactions. Data shown may be out of date.", Toast.LENGTH_LONG).show());
            }
        });
    }

    protected void startAuthenticationIntent() {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://auth.monzo.com/?" + "client_id=" + Constants.CLIENT_ID + "&redirect_uri=" + Constants.REDIRECT_URI + "&response_type=code"));
        Log.d(LOG_TAG, "Intent started");
        if (intent.resolveActivity(getContext().getPackageManager()) != null) {
            startActivity(intent);
        }

        monzoViewModel.getIsAuthenticatingData().postValue(true);
    }

}
