package com.example.csedcw.ui.transactions;

import android.graphics.Color;
import android.icu.text.DecimalFormat;
import android.icu.text.NumberFormat;
import android.icu.util.Currency;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import com.example.csedcw.R;
import com.example.csedcw.model.transactions.Transaction;

import static com.example.csedcw.Constants.LOG_TAG;

public class TransactionsAdapter extends ListAdapter<Transaction, TransactionsAdapter.TransactionViewHolder> {

    protected TransactionsAdapter() {
        super(new DiffUtil.ItemCallback<Transaction>() {
            @Override
            public boolean areItemsTheSame(@NonNull Transaction oldItem, @NonNull Transaction newItem) {
                return oldItem.getId().equals(newItem.getId());
            }

            @Override
            public boolean areContentsTheSame(@NonNull Transaction oldItem, @NonNull Transaction newItem) {
                return oldItem.getAmount() == newItem.getAmount() &&
                        oldItem.getDateTimeCreated().equals(newItem.getDateTimeCreated()) &&
                        oldItem.getCategory().equals(newItem.getCategory());
            }
        });
    }

    @NonNull
    @Override
    public TransactionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LinearLayout view = (LinearLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.transaction_item, parent, false);

        return new TransactionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TransactionViewHolder holder, int position) {
        Transaction transaction = getItem(position);

        int penceAmount = transaction.getAmount();

        BigDecimal poundsAbsoluteAmount = new BigDecimal(penceAmount).setScale(2, RoundingMode.HALF_DOWN).divide(new BigDecimal(100), RoundingMode.HALF_DOWN).abs();

        Currency currency = Currency.getInstance(transaction.getCurrency());

        NumberFormat formatter = DecimalFormat.getCurrencyInstance();
        formatter.setCurrency(currency);

        if (penceAmount >= 0) {
            holder.getAmountTextView().setText(formatter.format(poundsAbsoluteAmount));
            holder.getAmountTextView().setTextColor(Color.GREEN);
        } else {
            holder.getAmountTextView().setText(formatter.format(poundsAbsoluteAmount.negate()));
            holder.getAmountTextView().setTextColor(Color.RED);
        }

        LocalDate tranDate = transaction.getDateTimeCreated().toLocalDate();
        LocalDate nowDate = ZonedDateTime.now().toLocalDate();

        if (Math.abs(ChronoUnit.YEARS.between(tranDate, nowDate)) >= 1) {
            holder.getDateTextView().setText(transaction.getDateTimeCreated().format(DateTimeFormatter.ofPattern("u-L-d")));
        } else if (Math.abs(ChronoUnit.DAYS.between(tranDate, nowDate)) >= 5) {
            holder.getDateTextView().setText(transaction.getDateTimeCreated().format(DateTimeFormatter.ofPattern("LLL d',' H:mm")));
        } else if (transaction.getDateTimeCreated().toInstant().truncatedTo(ChronoUnit.DAYS).equals(Instant.now().truncatedTo(ChronoUnit.DAYS))) {
            holder.getDateTextView().setText(transaction.getDateTimeCreated().format(DateTimeFormatter.ofPattern("'Today' H:mm")));
        } else if (transaction.getDateTimeCreated().toInstant().truncatedTo(ChronoUnit.DAYS).equals(Instant.now().minus(Period.ofDays(1)).truncatedTo(ChronoUnit.DAYS))) {
            holder.getDateTextView().setText(transaction.getDateTimeCreated().format(DateTimeFormatter.ofPattern("'Y''Day' H:mm")));
        } else {
            holder.getDateTextView().setText(transaction.getDateTimeCreated().format(DateTimeFormatter.ofPattern("E H:mm")));
        }

        if (transaction.getCategory() != null && !transaction.getCategory().isEmpty()) {
            holder.getDescriptionTextView().setText(transaction.getCategory());
        } else {
            holder.getDescriptionTextView().setText("");
        }
    }

    public static class TransactionViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout textItemLayout;

        public TransactionViewHolder(@NonNull LinearLayout textItemLayout) {
            super(textItemLayout);
            this.textItemLayout = textItemLayout;
        }

        public TextView getAmountTextView() {
            return textItemLayout.findViewById(R.id.transaction_amount_text_view);
        }

        public TextView getDateTextView() {
            return textItemLayout.findViewById(R.id.transaction_date_text_view);
        }

        public TextView getDescriptionTextView() {
            return textItemLayout.findViewById(R.id.description_text_view);
        }
    }
}
