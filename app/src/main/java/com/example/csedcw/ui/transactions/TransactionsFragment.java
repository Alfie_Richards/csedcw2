package com.example.csedcw.ui.transactions;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.csedcw.R;
import com.example.csedcw.databinding.FragmentTransactionsBinding;
import com.example.csedcw.ui.MonzoFragment;

import static com.example.csedcw.Constants.LOG_TAG;

public class TransactionsFragment extends MonzoFragment {

    private FragmentTransactionsBinding binding;



    private TransactionsAdapter transactionsAdapter;

    private static final String UP_ARROW = " ↑";
    private static final String DOWN_ARROW = " ↓";

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        binding = FragmentTransactionsBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this.getContext());
        binding.transactionsRecyclerView.setLayoutManager(layoutManager);

        transactionsAdapter = new TransactionsAdapter();
        binding.transactionsRecyclerView.setAdapter(transactionsAdapter);

        transactionsViewModel.getTransactionsData().observe(getViewLifecycleOwner(), l -> {
            Log.d(LOG_TAG, l.toString());
            transactionsAdapter.submitList(l);

            if (l.isEmpty()) {
                binding.transactionsRecyclerView.setVisibility(View.GONE);
                binding.transactionsPlaceholderText.setVisibility(View.VISIBLE);
            } else {
                binding.transactionsRecyclerView.setVisibility(View.VISIBLE);
                binding.transactionsPlaceholderText.setVisibility(View.GONE);

                binding.transactionsRecyclerView.smoothScrollToPosition(0);
            }
        });

        binding.addTransactionFab.setOnClickListener(v -> {
            DialogFragment transactionDialog = new TransactionAddDialog();
            transactionDialog.show(requireActivity().getSupportFragmentManager(), "transaction_time_dialog");
        });

        monzoViewModel.getAccountIdData().observe(getViewLifecycleOwner(), newAccountId -> {
            if (newAccountId != null && !newAccountId.isEmpty()) {
                updateTransactions();
            }
        });

        binding.dateHeader.setText(getString(R.string.date_header_text, ""));
        binding.dateHeader.setOnClickListener(v -> {
            boolean opposite = transactionsViewModel.setCurrentSort(TransactionsViewModel.Sort.DATE);

            binding.amountHeader.setText(getString(R.string.amount_header_text, ""));
            binding.categoryHeader.setText(getString(R.string.category_header_text, ""));

            binding.dateHeader.setText(getString(R.string.date_header_text, opposite ? UP_ARROW : DOWN_ARROW));
        });

        binding.amountHeader.setText(getString(R.string.amount_header_text, ""));
        binding.amountHeader.setOnClickListener(v -> {
            boolean opposite = transactionsViewModel.setCurrentSort(TransactionsViewModel.Sort.AMOUNT);

            binding.dateHeader.setText(getString(R.string.date_header_text, ""));
            binding.categoryHeader.setText(getString(R.string.category_header_text, ""));

            binding.amountHeader.setText(getString(R.string.amount_header_text, opposite ? UP_ARROW : DOWN_ARROW));
        });

        binding.categoryHeader.setText(getString(R.string.category_header_text, ""));
        binding.categoryHeader.setOnClickListener(v -> {
            boolean opposite = transactionsViewModel.setCurrentSort(TransactionsViewModel.Sort.CATEGORY);

            binding.dateHeader.setText(getString(R.string.date_header_text, ""));
            binding.amountHeader.setText(getString(R.string.amount_header_text, ""));

            binding.categoryHeader.setText(getString(R.string.category_header_text, opposite ? UP_ARROW : DOWN_ARROW));
        });

        Pair<TransactionsViewModel.Sort, Boolean> currentSortConfig = transactionsViewModel.getCurrentSort();
        boolean opposite = currentSortConfig.second;

        if (currentSortConfig.first == TransactionsViewModel.Sort.CATEGORY) {
            binding.categoryHeader.setText(getString(R.string.category_header_text, opposite ? UP_ARROW : DOWN_ARROW));
        } else if (currentSortConfig.first == TransactionsViewModel.Sort.AMOUNT) {
            binding.amountHeader.setText(getString(R.string.amount_header_text, opposite ? UP_ARROW : DOWN_ARROW));
        } else if (currentSortConfig.first == TransactionsViewModel.Sort.DATE) {
            binding.dateHeader.setText(getString(R.string.date_header_text, opposite ? UP_ARROW : DOWN_ARROW));
        }

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(LOG_TAG, "destroy");
        binding = null;
    }

}