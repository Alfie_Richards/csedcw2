package com.example.csedcw.ui.transactions;

import android.app.Application;
import android.util.Log;
import android.util.Pair;

import androidx.annotation.NonNull;
import androidx.core.util.Consumer;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.csedcw.data.transactions.TransactionRepository;
import com.example.csedcw.model.transactions.Category;
import com.example.csedcw.model.transactions.Transaction;
import com.example.csedcw.monzo.MonzoAPI;
import com.example.csedcw.sorting.QuickSorter;
import com.example.csedcw.ui.MonzoViewModel;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.time.Period;
import java.time.ZonedDateTime;
import java.time.temporal.TemporalAmount;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.example.csedcw.Constants.LOG_TAG;

public class TransactionsViewModel extends AndroidViewModel {

    private MonzoAPI monzoAPI;

    private TransactionRepository transactionRepository;

    private static final Comparator<Transaction> categoryComparator = (o1, o2) -> o1.getCategory().compareTo(o2.getCategory());
    private static final Comparator<Transaction> amountComparator = (o1, o2) -> -Integer.compare(o1.getAmount(), o2.getAmount());
    private static final Comparator<Transaction> dateComparator = (o1, o2) -> -o1.getDateTimeCreated().compareTo(o2.getDateTimeCreated());

    enum Sort {
        CATEGORY(categoryComparator),
        AMOUNT(amountComparator),
        DATE(dateComparator);

        final Comparator<Transaction> comparator;
        final Comparator<Transaction> oppositeComparator;

        Sort(Comparator<Transaction> comparator) {
            this.comparator = comparator;
            this.oppositeComparator = (o1, o2) -> -comparator.compare(o1, o2);
        }
    }

    private QuickSorter<Transaction> quickSorter = new QuickSorter<>(Sort.DATE.comparator);
    private Pair<Sort, Boolean> currentSortConfig = new Pair<>(Sort.DATE, false); // (sort, isOpposite)

    private LiveData<List<Transaction>> localTransactions;
    private List<Transaction> remoteTransactionsCache = new ArrayList<>();

    private MutableLiveData<List<Transaction>> transactions = new MutableLiveData<>(new ArrayList<>());

    private MonzoViewModel monzoViewModel = null;

    public TransactionsViewModel(@NonNull Application application) {
        super(application);

        this.transactionRepository = new TransactionRepository(application);
        this.localTransactions = transactionRepository.getTransactionsData();
        localTransactions.observeForever(trs -> {
            Log.d(LOG_TAG, "LOC TRAN " + trs);

            if (trs != null) {
                trs.stream().filter(tr -> tr.getCreated().isEmpty()).forEach(tr -> transactionRepository.delete(tr));

                updateTransactions(trs, remoteTransactionsCache);
            }
        });

    }

    public void setMonzoViewModel(MonzoViewModel monzoViewModel) {
        this.monzoViewModel = monzoViewModel;
        this.monzoAPI = new MonzoAPI(monzoViewModel);
    }

    public boolean setCurrentSort(Sort sort) {
        boolean opposite = false;

        if (this.quickSorter.getComparator() == sort.comparator) {
            this.quickSorter.setComparator(sort.oppositeComparator);
            opposite = true;
        } else {
            this.quickSorter.setComparator(sort.comparator);
        }

        this.updateTransactions(localTransactions.getValue() != null ? localTransactions.getValue() : new ArrayList<>(), remoteTransactionsCache);

        this.currentSortConfig = new Pair<>(sort, opposite);

        return opposite;
    }

    public Pair<Sort, Boolean> getCurrentSort() {
        return currentSortConfig;
    }

    public void addLocalTransaction(Transaction transaction) {
        transactionRepository.insert(transaction);

        Log.d(LOG_TAG, "Adding local transaction " + transaction);
    }

    public void updateRemoteTransactions(Consumer<IOException> failureCallback) {
        if (monzoViewModel == null || !monzoViewModel.getIsAuthenticatedData().getValue()) {
            Log.d(LOG_TAG, "Didn't update remote transactions because not authenticated or Monzo viewmodel not set!");
            return;
        }

        monzoAPI.getTransactions(newRemote -> {
            remoteTransactionsCache = newRemote;
            transactionRepository.insertAll(newRemote);
            updateTransactions(localTransactions.getValue() != null ? localTransactions.getValue() : new ArrayList<>(), remoteTransactionsCache);
        }, failureCallback);
    }

    private void updateTransactions(List<Transaction> localTransactions, List<Transaction> remoteTransactionsCache) {
        List<Transaction> combined = Stream.concat(localTransactions.stream(), remoteTransactionsCache.stream()).distinct().collect(Collectors.toList());

        quickSorter.quicksort(combined, 0, combined.size() - 1);

        createRecurrences(combined);

        combined = combined.stream().distinct().collect(Collectors.toList());

        transactions.postValue(combined);
    }

    private void createRecurrences(List<Transaction> transactions) {
        new ArrayList<>(transactions).stream().filter(tr -> tr.getRecurrenceIndex() != 0).forEach(tr -> {
            Transaction.Recurrence recurrence = Transaction.Recurrence.values()[tr.getRecurrenceIndex()];
            TemporalAmount temporalAmount = recurrence.getTemporalAmount();
            TemporalAmount upperTemporalAmount;

            if (tr.getRecurrenceIndex() == Transaction.Recurrence.values().length - 1) {
                upperTemporalAmount = Period.ofYears(10);
            } else {
                upperTemporalAmount = Transaction.Recurrence.values()[tr.getRecurrenceIndex() + 1].getTemporalAmount();
            }

            List<Transaction> recurrences = new ArrayList<>();

            ZonedDateTime upperDateTime = ZonedDateTime.now().plus(upperTemporalAmount);
            ZonedDateTime currentDateTime = tr.getDateTimeCreated().plus(temporalAmount);

            for(int i = 1; currentDateTime.isBefore(upperDateTime); i++) {
                recurrences.add(new Transaction(tr.getId() + i, tr.getCurrency(), tr.getCategory(), tr.getAmount(), currentDateTime.toString(), 0));
                currentDateTime = currentDateTime.plus(temporalAmount);
            }

            transactions.addAll(recurrences);
            transactionRepository.insertAll(recurrences);
        });
    }

    public LiveData<List<Transaction>> getTransactionsData() {
        return transactions;
    }

    public LiveData<List<Transaction>> getTransactionsInCategoriesData(List<Category> categories) {
        return transactionRepository.getTransactionsInCategoriesData(categories);
    }
}