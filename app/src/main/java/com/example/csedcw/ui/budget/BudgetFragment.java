package com.example.csedcw.ui.budget;

import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModelProvider;

import com.example.csedcw.R;
import com.example.csedcw.databinding.BudgetFragmentBinding;
import com.example.csedcw.model.transactions.Category;
import com.example.csedcw.model.transactions.TimePeriod;
import com.example.csedcw.model.transactions.Transaction;
import com.example.csedcw.ui.transactions.TransactionsViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.example.csedcw.Constants.LOG_TAG;

public class BudgetFragment extends Fragment {

    private BudgetViewModel budgetViewModel;

    private TransactionsViewModel transactionsViewModel;

    private BudgetFragmentBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        binding = BudgetFragmentBinding.inflate(inflater);
        View view = binding.getRoot();

        transactionsViewModel = new ViewModelProvider(requireActivity()).get(TransactionsViewModel.class);

        budgetViewModel = new ViewModelProvider(requireActivity()).get(BudgetViewModel.class);

        TimePeriodPagerAdapter timePeriodPagerAdapter = new TimePeriodPagerAdapter(getContext(), getChildFragmentManager());

        binding.viewPager.setAdapter(timePeriodPagerAdapter);
        binding.tabs.setupWithViewPager(binding.viewPager);

        Transformations.switchMap(budgetViewModel.getCategoriesData(),
                categories -> Transformations.switchMap(transactionsViewModel.getTransactionsData(), trans -> Transformations.map(budgetViewModel.getTimePeriodData(), periods -> new Pair<>(categories, new Pair<>(periods, trans)))))
                .observe(getViewLifecycleOwner(), pair -> {
                    List<Category> categories = pair.first;
                    List<TimePeriod> timePeriods = pair.second.first;
                    List<Transaction> transactions = pair.second.second;

                    categories.stream()
                            .peek(c -> Log.d(LOG_TAG, "Cat: " + c.getName()))
                            .mapToDouble(cat -> {
                                TimePeriod timePeriod = timePeriods.stream().filter(period -> period.getName().equals(cat.getTimePeriodName())).findFirst().orElseThrow(() -> new IllegalStateException("Category has unknown time period"));

                                Log.d(LOG_TAG, "Time p: " + timePeriod.getName());

                                return 100 * (1 - (double)transactions.stream()
                                        .filter(tr -> tr.getDateTimeCreated().toLocalDateTime().isAfter(timePeriod.getStartDate().atStartOfDay()) &&
                                                tr.getDateTimeCreated().toLocalDateTime().isBefore(timePeriod.getEndDate().atTime(23, 59)))
                                        .filter(tr -> tr.getCategory().equals(cat.getName())).mapToInt(Transaction::getAmountSpent)
                                        .sum() / cat.getTotalBudgetPence());
                            })
                            .peek(i -> Log.d(LOG_TAG, "Av: " + i))
                            .average()
                            .ifPresent(averagePercentage -> {
                                if (averagePercentage >= 75) {
                                    binding.badgeImage.setImageDrawable(requireActivity().getDrawable(R.drawable.gold_badge));
                                    binding.badgeText.setText(R.string.gold_label);
                                } else if (averagePercentage >= 50) {
                                    binding.badgeImage.setImageDrawable(requireActivity().getDrawable(R.drawable.silver_badge));
                                    binding.badgeText.setText(R.string.silver_label);
                                } else {
                                    binding.badgeImage.setImageDrawable(requireActivity().getDrawable(R.drawable.bronze_badge));
                                    binding.badgeText.setText(R.string.bronze_label);
                                }

                                Log.d(LOG_TAG, "Average saving percentage: " + averagePercentage);
                            });
                });

        return view;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();

        binding = null;
    }
}
