package com.example.csedcw.ui;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class MonzoViewModel extends ViewModel {

    private MutableLiveData<Boolean> isAuthenticating = new MutableLiveData<Boolean>(false);
    private MutableLiveData<Boolean> isAuthenticated = new MutableLiveData<Boolean>(false);

    private MutableLiveData<String> accessToken = new MutableLiveData<>("");
    private MutableLiveData<String> refreshToken = new MutableLiveData<>("");
    private MutableLiveData<String> accountId = new MutableLiveData<>("");
    private MutableLiveData<Integer> expiresInSeconds = new MutableLiveData<>(-1);

    public MonzoViewModel() {
        accessToken.observeForever(newToken -> {
            if (newToken != null && !newToken.isEmpty()) {
                isAuthenticating.setValue(false);
                isAuthenticated.setValue(true);
            }
        });
    }

    public MutableLiveData<Boolean> getIsAuthenticatedData() {
        return isAuthenticated;
    }

    public MutableLiveData<Boolean> getIsAuthenticatingData() {
        return isAuthenticating;
    }

    public MutableLiveData<String> getAccessTokenData() {
        return accessToken;
    }

    public MutableLiveData<String> getRefreshTokenData() {
        return refreshToken;
    }

    public MutableLiveData<String> getAccountIdData() {
        return accountId;
    }

    public MutableLiveData<Integer> getExpiresInSecondsData() {
        return expiresInSeconds;
    }
}
