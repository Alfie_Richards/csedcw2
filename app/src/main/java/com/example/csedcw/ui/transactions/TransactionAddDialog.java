package com.example.csedcw.ui.transactions;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.csedcw.R;
import com.example.csedcw.ui.budget.BudgetViewModel;

import java.util.ArrayList;
import java.util.List;

public class TransactionAddDialog extends DialogFragment {

    public interface TransactionAddDialogListener {
        void onTransactionDialogPositiveClick(DialogFragment dialog, EditText amountEditText, Spinner categoryEditText, Spinner recurringSpinner, DatePicker datePicker, TimePicker timePicker);

        void onTransactionDialogNegativeClick(DialogFragment dialog);
    }

    private TransactionAddDialogListener listener;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        try {
            listener = (TransactionAddDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(requireActivity().toString() + "must implement TransactionAddDialogListener");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = requireActivity().getLayoutInflater().inflate(R.layout.transaction_add_dialog, null);
        builder.setView(view);

        EditText amountEditText = view.findViewById(R.id.amount_edit_text);
        Spinner categoryEditText = view.findViewById(R.id.category_spinner);

        List<String> categories = new ArrayList<>();
        (new ViewModelProvider(requireActivity()).get(BudgetViewModel.class)).getCategoriesData().getValue().forEach(category -> {
            categories.add(category.getName());
        });

        categoryEditText.setAdapter(new ArrayAdapter(getContext(), R.layout.support_simple_spinner_dropdown_item, categories));

        DatePicker datePicker = view.findViewById(R.id.transaction_date_picker);
        TimePicker timePicker = view.findViewById(R.id.transaction_time_picker);

        Spinner recurringSpinner = view.findViewById(R.id.recurring_spinner);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this.requireContext(), R.array.recurring_items, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        recurringSpinner.setAdapter(adapter);

        builder.setPositiveButton("Confirm", (dialog, id) -> listener.onTransactionDialogPositiveClick(TransactionAddDialog.this, amountEditText, categoryEditText, recurringSpinner, datePicker, timePicker));
        builder.setNegativeButton("Cancel", (dialog, id) -> listener.onTransactionDialogNegativeClick(TransactionAddDialog.this));

        return builder.create();
    }
}
