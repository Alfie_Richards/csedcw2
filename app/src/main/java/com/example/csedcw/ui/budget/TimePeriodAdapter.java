package com.example.csedcw.ui.budget;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.csedcw.model.transactions.Category;
import com.example.csedcw.R;
import com.example.csedcw.model.transactions.TimePeriod;
import com.example.csedcw.model.transactions.Transaction;
import com.example.csedcw.ui.formatting.CurrencyFormatter;
import com.example.csedcw.ui.transactions.TransactionsViewModel;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.example.csedcw.Constants.LOG_TAG;

public class TimePeriodAdapter extends RecyclerView.Adapter<TimePeriodAdapter.MyViewHolder> {

    private TimePeriod timePeriod;
    private List<Category> categories = new ArrayList<>();
    private Map<Category, List<Transaction>> categoryTransactions = new HashMap<>();

    private TransactionsViewModel transactionsViewModel;
    private BudgetViewModel budgetViewModel;

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewTitle;
        public TextView textViewBudgetGone;
        public TextView textViewBudgetRemaining;

        public ProgressBar progressBarSpent;

        public MyViewHolder(View view) {
            super(view);
            textViewTitle = view.findViewById(R.id.category_name);
            textViewBudgetGone = view.findViewById(R.id.month_progress_1);
            textViewBudgetRemaining = view.findViewById(R.id.month_progress_2);
            progressBarSpent = view.findViewById(R.id.spent_progress);
        }
    }

    private Context context;

    public TimePeriodAdapter(TimePeriod timePeriod, Context context, TransactionsViewModel transactionsViewModel, BudgetViewModel budgetViewModel) {
        this.timePeriod = timePeriod;
        this.context = context;
        this.transactionsViewModel = transactionsViewModel;
        this.budgetViewModel = budgetViewModel;
    }

    public void submitCategories(List<Category> categories) {
        this.categories = categories;
        this.notifyDataSetChanged();
    }

    public void submitCategoryTransactions(Map<Category, List<Transaction>> categoryTransactions) {
        this.categoryTransactions = categoryTransactions;
        this.notifyDataSetChanged();

        Log.d(LOG_TAG, "Cat, Tran: " + categoryTransactions);
    }

    @Override
    public TimePeriodAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                             int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_summary, parent, false);
        MyViewHolder viewHolder = new MyViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        int amountSpent = categoryTransactions.getOrDefault(categories.get(position), new ArrayList<>()).stream()
                .filter(tr -> tr.getDateTimeCreated().toLocalDateTime().isAfter(timePeriod.getStartDate().atStartOfDay()) &&
                        tr.getDateTimeCreated().toLocalDateTime().isBefore(timePeriod.getEndDate().atTime(23, 59)))
                .mapToInt(Transaction::getAmountSpent).sum();
        int totalBudget = categories.get(position).getTotalBudgetPence();

        if (amountSpent < 0) {
            amountSpent = 0;
        }

        holder.textViewTitle.setText(categories.get(position).getName());
        holder.textViewBudgetRemaining.setText(context.getString(R.string.left_placeholder, CurrencyFormatter.format(new BigDecimal(totalBudget - amountSpent).divide(new BigDecimal(100), 2, RoundingMode.HALF_UP))));
        if (totalBudget - amountSpent <= 0) {
            holder.textViewBudgetRemaining.setTextColor(Color.RED);
        }

        holder.textViewBudgetGone.setText(context.getString(R.string.spent_placeholder, CurrencyFormatter.format(new BigDecimal(amountSpent).divide(new BigDecimal(100), 2, RoundingMode.HALF_UP))));
        holder.progressBarSpent.setProgress(100 * amountSpent / totalBudget);
        holder.progressBarSpent.setMax(100);

        holder.textViewTitle.setOnClickListener(view -> {
            Log.d("Budget", "Launch category screen");
        });
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }
}
