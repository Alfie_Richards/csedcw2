package com.example.csedcw.ui.balance;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.csedcw.Constants;
import com.example.csedcw.R;
import com.example.csedcw.model.transactions.Category;
import com.example.csedcw.model.transactions.TimePeriod;
import com.example.csedcw.ui.budget.BudgetViewModel;
import com.example.csedcw.ui.formatting.Formatters;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class BalanceSheetCatAdapter extends RecyclerView.Adapter<BalanceSheetCatAdapter.MyViewHolder> {

    private TimePeriod timePeriod;
    private BalanceSheetTFAdapter parent;
    private BudgetViewModel budgetViewModel;

    private List<Category> categories;

    BalanceSheetCatAdapter(TimePeriod timePeriod, BudgetViewModel budgetViewModel, BalanceSheetTFAdapter parent) {
        this.timePeriod = timePeriod;
        this.parent = parent;
        this.budgetViewModel = budgetViewModel;

        categories = new ArrayList<>();

        budgetViewModel.getCategoriesData().getValue().forEach(category -> {
            if(category.getTimePeriodName().equals(timePeriod.getName())) {
                categories.add(category);
            }
        });

    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView categoryName, minusButton, plusButton;
        EditText percentageText, moneyAmountText;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);

            categoryName = itemView.findViewById(R.id.categoryName);

            minusButton = itemView.findViewById(R.id.minusButton);
            plusButton = itemView.findViewById(R.id.plusButton);

            percentageText = itemView.findViewById(R.id.percentageText);
            moneyAmountText = itemView.findViewById(R.id.moneyAmountText);
        }
    }

    @NotNull
    @Override
    public BalanceSheetCatAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                                 int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_balance_section, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        Category category = categories.get(position);

        holder.categoryName.setText(category.getName());

        holder.percentageText.setText(Formatters.percentageFormatter((int) (((long) 10000) * category.getTotalBudgetPence() * timePeriod.getNumPerYear() / budgetViewModel.getBudget())));
        holder.moneyAmountText.setText(Formatters.moneyFormatter(category.getTotalBudgetPence()));

        holder.moneyAmountText.setOnFocusChangeListener((v, hasFocus) -> {
            if(!hasFocus) {
                EditText editText = (EditText) v;

                int result = Formatters.parseMoneyString(editText.getText().toString());

                if (result != -1) {
                    if (result != category.getTotalBudgetPence()) {
                        Log.d(Constants.LOG_TAG, String.format("result = %d", result));
                        category.setBudget(result);
                        parent.balanceSheet.update();
                    }
                } else {
                    holder.moneyAmountText.setText(Formatters.moneyFormatter(category.getTotalBudgetPence()));
                    Toast toast = Toast.makeText(holder.percentageText.getContext(), "Invalid input.", Toast.LENGTH_LONG);
                    toast.show();
                }
            }
        });

        holder.percentageText.setOnFocusChangeListener((v, hasFocus) -> {
            if(!hasFocus) {
                EditText editText = (EditText) v;

                int result = Formatters.parsePercentageString(editText.getText().toString());

                if (result != -1) {
                    if (result != category.getTotalBudgetPence()) {
                        Log.d(Constants.LOG_TAG, String.format("result = %d", result));
                        category.setBudget((int) Math.round(result * budgetViewModel.getBudget() * 1.0 / timePeriod.getNumPerYear() / 10000));
                        parent.balanceSheet.update();
                    }
                } else {
                    holder.percentageText.setText(Formatters.percentageFormatter((int) (((long) 10000) * category.getTotalBudgetPence() * timePeriod.getNumPerYear() / budgetViewModel.getBudget())));

                    Toast toast = Toast.makeText(holder.percentageText.getContext(), "Invalid input.", Toast.LENGTH_LONG);
                    toast.show();
                }
            }
        });

        holder.plusButton.setOnClickListener(v -> {
            category.setBudget((int) Math.round(Math.max((((long) 10000) * category.getTotalBudgetPence() * timePeriod.getNumPerYear() / budgetViewModel.getBudget()) + 100, 0) * budgetViewModel.getBudget() * 1.0 / timePeriod.getNumPerYear() / 10000));
            parent.balanceSheet.update();
        });

        holder.minusButton.setOnClickListener(v -> {
            category.setBudget((int) Math.round(Math.max((((long) 10000) * category.getTotalBudgetPence() * timePeriod.getNumPerYear() / budgetViewModel.getBudget()) - 100, 0) * budgetViewModel.getBudget() * 1.0 / timePeriod.getNumPerYear() / 10000));
            parent.balanceSheet.update();
        });


        holder.categoryName.setOnClickListener(view -> {
            EditCategoryFragment editCategoryFragment = EditCategoryFragment.newInstance(category.getName(), category.getTotalBudgetPence(), timePeriod.getName());
            editCategoryFragment.show(parent.balanceSheet.requireActivity().getSupportFragmentManager(), "fragment_add_manager");
        });
    }

    @Override
    public int getItemCount() {
        Log.d("Balance", "Size Cat's: " + categories.size());
        return categories.size();
    }
}
