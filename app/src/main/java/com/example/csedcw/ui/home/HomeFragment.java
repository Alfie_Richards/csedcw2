package com.example.csedcw.ui.home;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.icu.text.DecimalFormat;
import android.icu.text.NumberFormat;
import android.icu.util.Currency;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.example.csedcw.R;
import com.example.csedcw.databinding.FragmentHomeBinding;
import com.example.csedcw.model.transactions.Transaction;
import com.example.csedcw.model.transactions.visualisation.DataFormatter;
import com.example.csedcw.model.transactions.visualisation.GraphTime;
import com.example.csedcw.ui.MonzoFragment;
import com.example.csedcw.ui.formatting.CurrencyFormatter;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.google.android.material.resources.TextAppearance;

import static com.example.csedcw.Constants.LOG_TAG;

public class HomeFragment extends MonzoFragment {

    private HomeViewModel homeViewModel;

    private FragmentHomeBinding binding;

    private ValueFormatter dataFormatter = new DataFormatter();

    private GraphTime currentGraphTime = GraphTime.WEEK;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        binding = FragmentHomeBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();

        binding.connectMonzoButton.setOnClickListener(v -> startAuthenticationIntent());

        homeViewModel = new ViewModelProvider(requireActivity()).get(HomeViewModel.class);
        homeViewModel.setMonzoViewModel(monzoViewModel);

        homeViewModel.getBalance().observe(getViewLifecycleOwner(), bal -> {
            if (bal != null) {
                binding.balanceText.setVisibility(View.VISIBLE);
                binding.connectMonzoButton.setVisibility(View.GONE);

                binding.balanceText.setText(getString(R.string.balance_display, CurrencyFormatter.format(bal.divide(new BigDecimal(100), RoundingMode.HALF_DOWN))));
            }
        });

        monzoViewModel.getAccessTokenData().observe(getViewLifecycleOwner(), newToken -> {
            if (newToken != null && !newToken.isEmpty()) {
                updateBalance();
            }
        });

        monzoViewModel.getAccountIdData().observe(getViewLifecycleOwner(), newAccountId -> {
            if (newAccountId != null && !newAccountId.isEmpty()) {
                updateBalance();
            }
        });


        transactionsViewModel.getTransactionsData().observe(getViewLifecycleOwner(), this::updateGraphData);

        binding.periodBarChart.getDescription().setEnabled(false);
        binding.periodBarChart.getXAxis().setValueFormatter(currentGraphTime.xAxisFormatter);
        binding.periodBarChart.getAxisLeft().setValueFormatter(dataFormatter);
        binding.periodBarChart.getAxisRight().setDrawLabels(false);
        binding.periodBarChart.getXAxis().setGranularity(1);
        binding.periodBarChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM_INSIDE);

        binding.weekGraphButton.setOnClickListener(v -> {
            currentGraphTime = GraphTime.WEEK;

            binding.weekGraphButton.setTextSize(25);
            binding.weekGraphButton.setTypeface(binding.weekGraphButton.getTypeface(), Typeface.BOLD);

            binding.monthGraphButton.setTextSize(20);
            binding.monthGraphButton.setTypeface(binding.monthGraphButton.getTypeface(), Typeface.NORMAL);

            binding.yearGraphButton.setTextSize(20);
            binding.yearGraphButton.setTypeface(binding.yearGraphButton.getTypeface(), Typeface.NORMAL);

            updateGraphData(transactionsViewModel.getTransactionsData().getValue() != null ? transactionsViewModel.getTransactionsData().getValue() : new ArrayList<>());
        });

        binding.monthGraphButton.setOnClickListener(v -> {
            currentGraphTime = GraphTime.MONTH;

            binding.weekGraphButton.setTextSize(20);
            binding.weekGraphButton.setTypeface(binding.weekGraphButton.getTypeface(), Typeface.NORMAL);

            binding.monthGraphButton.setTextSize(25);
            binding.monthGraphButton.setTypeface(binding.monthGraphButton.getTypeface(), Typeface.BOLD);

            binding.yearGraphButton.setTextSize(20);
            binding.yearGraphButton.setTypeface(binding.yearGraphButton.getTypeface(), Typeface.NORMAL);

            updateGraphData(transactionsViewModel.getTransactionsData().getValue() != null ? transactionsViewModel.getTransactionsData().getValue() : new ArrayList<>());
        });

        binding.yearGraphButton.setOnClickListener(v -> {
            currentGraphTime = GraphTime.YEAR;

            binding.weekGraphButton.setTextSize(20);
            binding.weekGraphButton.setTypeface(binding.weekGraphButton.getTypeface(), Typeface.NORMAL);

            binding.monthGraphButton.setTextSize(20);
            binding.monthGraphButton.setTypeface(binding.monthGraphButton.getTypeface(), Typeface.NORMAL);

            binding.yearGraphButton.setTextSize(25);
            binding.yearGraphButton.setTypeface(binding.yearGraphButton.getTypeface(), Typeface.BOLD);

            updateGraphData(transactionsViewModel.getTransactionsData().getValue() != null ? transactionsViewModel.getTransactionsData().getValue() : new ArrayList<>());
        });

        return view;
    }

    private void updateGraphData(List<Transaction> transactions) {
        List<BarEntry> entries = new ArrayList<>();

        Map<Integer, Double> spendingData = currentGraphTime.dataFunction.apply(transactions);

        Log.d(LOG_TAG, "Sp Data: " + spendingData);

        for (int i = 1; i <= currentGraphTime.maxMagnitude; i++) {
            spendingData.putIfAbsent(i, 0d); // Populate with default values
        }

        spendingData.entrySet().forEach(entry -> entries.add(new BarEntry(entry.getKey().floatValue(), entry.getValue().floatValue())));

        BarDataSet dataSet = new BarDataSet(entries, "Amount Spent (£)");
        dataSet.setColor(Color.RED);
        dataSet.setValueTextColor(Color.BLACK);

        BarData data = new BarData(dataSet);
        data.setValueFormatter(dataFormatter);

        binding.periodBarChart.setData(data);

        binding.periodBarChart.getXAxis().setValueFormatter(currentGraphTime.xAxisFormatter);

        binding.periodBarChart.invalidate();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (monzoConnected) {
            binding.balanceText.setVisibility(View.VISIBLE);
            binding.connectMonzoButton.setVisibility(View.GONE);
        } else {
            binding.balanceText.setVisibility(View.GONE);
            binding.connectMonzoButton.setVisibility(View.VISIBLE);
        }

        updateBalance();
    }

    private void updateBalance() {
        if (monzoViewModel.getIsAuthenticatedData().getValue()) {
            homeViewModel.updateBalance(ex -> {
                Activity activity = getActivity();
                if (activity != null && ex != null) {
                    activity.runOnUiThread(() -> Toast.makeText(activity, ex.getMessage(), Toast.LENGTH_LONG).show());
                }
            });
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }


}