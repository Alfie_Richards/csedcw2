package com.example.csedcw.ui.formatting;

import android.icu.text.DecimalFormat;
import android.icu.text.NumberFormat;
import android.icu.util.Currency;

import java.math.BigDecimal;

public class CurrencyFormatter {
    private static final Currency currency = Currency.getInstance("GBP");

    private static final NumberFormat formatter = DecimalFormat.getCurrencyInstance();

    static {
        formatter.setCurrency(currency);
    }

    public static String format(BigDecimal num) {
        return formatter.format(num);
    }
}
