package com.example.csedcw.ui.formatting;

import android.util.Log;

import com.example.csedcw.Constants;

import java.util.Locale;

public class Formatters {
    public static String moneyFormatter(int amount) {
        return String.format(Locale.UK, (amount < 0 ? "-" : "") + "£%d.%02d", Math.abs(amount) / 100, Math.abs(amount) % 100);
    }

    public static String percentageFormatter(long amount) {
        return String.format(Locale.UK, "%.2f%%", amount * 1.0 / 100);
    }

    // returns -1 on an error
    public static int parseMoneyString(String string) {
        string = string.replaceAll(" ", "");
        string = string.replaceAll("[^0-9.]", "");

        String[] strings = string.split("[^0-9]");

        // Log.d(Constants.LOG_TAG, "Value: " + string);
        for(String s: strings) {
            // Log.d(Constants.LOG_TAG, "Value: " + s);
        }

        if (strings.length == 1) {
            return Integer.parseInt(strings[0]) * 100;
        } else if (strings.length == 2) {
            if(strings[1].length() == 2) {
                return Integer.parseInt(strings[0]) * 100 + Integer.parseInt(strings[1]);
            } else if (strings[1].length() == 1) {
                return  Integer.parseInt(strings[0]) * 100 + Integer.parseInt(strings[1]) * 10;
            } else {
                return -1;
            }
        } else {
            return -1;
        }
    }

    // returns -1 on an error
    public static int parsePercentageString(String string) {
        string = string.replaceAll(" ", "");
        string = string.replaceAll("[^0-9.]", "");

        String[] strings = string.split("[^0-9]");

        // Log.d(Constants.LOG_TAG, "Value: " + string);
        // for(String s: strings) {
            // Log.d(Constants.LOG_TAG, "Value: " + s);
        // }

        if (strings.length == 1) {
            return Integer.parseInt(strings[0]) * 100;
        } else if (strings.length == 2) {
            if(strings[1].length() == 2) {
                return Integer.parseInt(strings[0]) * 100 + Integer.parseInt(strings[1]);
            } else if (strings[1].length() == 1) {
                return  Integer.parseInt(strings[0]) * 100 + Integer.parseInt(strings[1]) * 10;
            } else {
                return -1;
            }
        } else {
            return -1;
        }
    }
}
