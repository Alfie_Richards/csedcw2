package com.example.csedcw.ui.budget;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.csedcw.R;
import com.example.csedcw.databinding.TimePeriodFragmentBinding;
import com.example.csedcw.model.transactions.Category;
import com.example.csedcw.model.transactions.TimePeriod;
import com.example.csedcw.model.transactions.Transaction;
import com.example.csedcw.ui.MonzoViewModel;
import com.example.csedcw.ui.formatting.CurrencyFormatter;
import com.example.csedcw.ui.transactions.TransactionsViewModel;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.example.csedcw.Constants.LOG_TAG;

public class TimePeriodFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";

    private TransactionsViewModel transactionsViewModel;

    private BudgetViewModel budgetViewModel;

    private Map<Category, List<Transaction>> categoryTransactions = new HashMap<>();

    private TimePeriodFragmentBinding binding;

    private TimePeriodAdapter adapter;

    private int index;

    public static TimePeriodFragment newInstance(int timePeriodIndex) {
        TimePeriodFragment fragment = new TimePeriodFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, timePeriodIndex);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        binding = TimePeriodFragmentBinding.inflate(getLayoutInflater());
        View root = binding.getRoot();

        Log.d(LOG_TAG, "CREATE: " + index);

        transactionsViewModel = new ViewModelProvider(requireActivity()).get(TransactionsViewModel.class);

        budgetViewModel = new ViewModelProvider(requireActivity()).get(BudgetViewModel.class);

        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
            System.out.println("Index: " + index);
        }

        TimePeriod timePeriod = budgetViewModel.getTimePeriod(index);

        binding.Title.setText(timePeriod.getName());
        binding.progressBar.setMax(100);

        int timeSpent = (int) ChronoUnit.DAYS.between(timePeriod.getStartDate().atStartOfDay(), LocalDate.now().atStartOfDay());
        int timeLeft = (int) ChronoUnit.DAYS.between(LocalDate.now().atStartOfDay(), timePeriod.getEndDate().atStartOfDay()) + 1;

        binding.progressBarTime.setMax(365 / timePeriod.getNumPerYear());

        binding.progressBarTime.setProgress(timeSpent);

        binding.textTimeSpent.setText(getString(R.string.days_spent_placeholder, timeSpent));
        binding.textTimeLeft.setText(getString(R.string.days_left_placeholder, timeLeft));

        adapter = new TimePeriodAdapter(timePeriod, getContext(), transactionsViewModel, budgetViewModel);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));
        binding.recyclerView.setAdapter(adapter);

        Transformations.switchMap(
                budgetViewModel.getCategoriesData(),
                categories -> {
                    List<Category> categoriesInPeriod = categories.stream()
                            .filter(cat -> cat.getTimePeriodName().equals(timePeriod.getName()))
                            .collect(Collectors.toList());

                    return Transformations.map(transactionsViewModel.getTransactionsInCategoriesData(categoriesInPeriod), trans -> new Pair<>(new ArrayList<>(categoriesInPeriod), trans));
                }).observe(getViewLifecycleOwner(), pair -> {

            List<Category> categoriesInPeriod = pair.first;
            List<Transaction> transactionsInCategories = pair.second;

            int totalSpent = Math.abs(transactionsInCategories.stream()
                    .filter(tr -> tr.getDateTimeCreated().toLocalDateTime().isAfter(timePeriod.getStartDate().atStartOfDay()) &&
                            tr.getDateTimeCreated().toLocalDateTime().isBefore(timePeriod.getEndDate().atTime(23, 59)))
                    .mapToInt(Transaction::getAmountSpent)
                    .sum());

            int totalBudget = categoriesInPeriod.stream()
                    .mapToInt(Category::getTotalBudgetPence)
                    .sum();

            Log.d(LOG_TAG, "Spent: " + totalSpent);
            Log.d(LOG_TAG, "Trans" + transactionsInCategories);
            Log.d(LOG_TAG, "Cats: " + categoriesInPeriod.stream().map(Category::getName).collect(Collectors.joining(", ")));

            binding.textSpent.setText(getString(R.string.spent_placeholder, CurrencyFormatter.format(new BigDecimal(totalSpent).divide(new BigDecimal(100), 2, RoundingMode.HALF_UP))));
            if (totalBudget - totalSpent <= 0) {
                binding.textSpent.setTextColor(Color.rgb(225, 0, 0));
            }

            binding.textBudget.setText(getString(R.string.budget_placeholder, CurrencyFormatter.format(new BigDecimal(totalBudget).divide(new BigDecimal(100), 2, RoundingMode.HALF_DOWN))));
            binding.progressBar.setProgress(totalBudget == 0 ? 0 : 100 * totalSpent / totalBudget);

            adapter.submitCategories(categoriesInPeriod);

            categoryTransactions = transactionsInCategories.stream()
                    .collect(Collectors.groupingBy(tr -> categoriesInPeriod.stream().filter(c -> c.getName().equals(tr.getCategory())).findFirst().orElseThrow(IllegalStateException::new), Collectors.toList()));

            adapter.submitCategoryTransactions(categoryTransactions);
        });

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();

        transactionsViewModel.updateRemoteTransactions(ex -> {
            Activity activity = getActivity();
            if (activity != null) {
                activity.runOnUiThread(() -> Toast.makeText(activity, "Unable to update transactions. Data shown may be out of date.", Toast.LENGTH_LONG).show());
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        Log.d(LOG_TAG, "DESTROY: " + index);

        binding = null;
    }
}