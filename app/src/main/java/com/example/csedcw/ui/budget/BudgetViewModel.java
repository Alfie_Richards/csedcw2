package com.example.csedcw.ui.budget;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.csedcw.data.transactions.CategoryRepository;
import com.example.csedcw.model.transactions.Category;
import com.example.csedcw.model.transactions.TimePeriod;
import com.example.csedcw.model.transactions.Transaction;
import com.example.csedcw.ui.transactions.TransactionsViewModel;

import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static com.example.csedcw.Constants.LOG_TAG;

public class BudgetViewModel extends AndroidViewModel {

    private CategoryRepository categoryRepository;

    private MutableLiveData<List<Category>> categories = new MutableLiveData<>(new ArrayList<>());
    private MutableLiveData<List<TimePeriod>> timePeriods = new MutableLiveData<>(new ArrayList<>());

    private int budget;

    public BudgetViewModel(Application application) {
        super(application);

        Log.d("Budget View Model", "Made budget view");

        categoryRepository = new CategoryRepository(application);
        TimePeriod week = new TimePeriod("Week", d -> d.getDayOfWeek().equals(DayOfWeek.MONDAY), 52);
        TimePeriod month = new TimePeriod("Month", d -> d.getDayOfMonth() == 1, 12);
        TimePeriod year = new TimePeriod("Year", d -> d.getDayOfYear() == 1, 1);

        timePeriods.setValue(Arrays.asList(week, month, year));

        if(categoryRepository.getCategories().getValue() == null || categoryRepository.getCategories().getValue().size() == 0) {
            budget = 1000000;

            Log.d("Budget View Model", "Made categories");

            categoryRepository.insert(new Category("Food", week.getName(), 3500));
            categoryRepository.insert(new Category("Nights out", week.getName(), 1500));
            categoryRepository.insert(new Category("Travel", month.getName(), 6000));
            categoryRepository.insert(new Category("Phone Bill", month.getName(), 1500));
            categoryRepository.insert(new Category("Gym", year.getName(), 20000));
            categoryRepository.insert(new Category("Rent", year.getName(), 400000));
        }

        categoryRepository.getCategories().observeForever(categories -> {
            Log.d(LOG_TAG, "Categories: " + categories);
            this.categories.setValue(categories);
        });
    }

    public TimePeriod getTimePeriod(int i) {
        return timePeriods.getValue().get(i);
    }

    public LiveData<List<Category>> getCategoriesData() {
        return categories;
    }

    public LiveData<List<TimePeriod>> getTimePeriodData() {
        return timePeriods;
    }

    public int getBudget() {
        return budget;
    }

    public void setBudget(int budget) {
        this.budget = budget;
    }

    public int getTotalBudget() {
        return categories.getValue().stream().mapToInt(Category::getTotalBudgetPence).sum();
    }

    public void addCategory(String name, int budget, String timeFrame) {
        categoryRepository.insert(new Category(name, timeFrame, budget));
    }

    public void deleteCategory(String categoryName) {

        for (Category category: categories.getValue()) {
            if (categoryName.equals(category.getName())) {
                categoryRepository.remove(category);
            }
        }
    }
}
