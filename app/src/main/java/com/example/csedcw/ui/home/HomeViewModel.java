package com.example.csedcw.ui.home;

import android.util.Log;

import androidx.core.util.Consumer;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.csedcw.monzo.MonzoAPI;

import java.io.IOException;
import java.math.BigDecimal;

import com.example.csedcw.ui.MonzoViewModel;

import static com.example.csedcw.Constants.LOG_TAG;

public class HomeViewModel extends ViewModel {

    private MutableLiveData<BigDecimal> balance;

    private MonzoAPI monzoAPI;

    private MonzoViewModel monzoViewModel = null;

    public HomeViewModel() {
        balance = new MutableLiveData<>();
    }

    public void setMonzoViewModel(MonzoViewModel monzoViewModel) {
        this.monzoViewModel = monzoViewModel;
        this.monzoAPI = new MonzoAPI(monzoViewModel);
    }

    public void updateBalance(Consumer<IOException> failureCallBack) {
        if (monzoViewModel == null || !monzoViewModel.getIsAuthenticatedData().getValue()) {
            return;
        }

        monzoAPI.getBalancePence(bal -> {
            Log.d(LOG_TAG, "New balance coming of " + bal);
            balance.postValue(bal);
        }, failureCallBack);
    }

    public void setBalance(BigDecimal balance) {
        this.balance.setValue(balance);
    }

    public LiveData<BigDecimal> getBalance() {
        return this.balance;
    }
}