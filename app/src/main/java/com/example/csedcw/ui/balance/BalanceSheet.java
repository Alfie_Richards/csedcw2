package com.example.csedcw.ui.balance;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.csedcw.Constants;
import com.example.csedcw.R;
import com.example.csedcw.databinding.FragmentBalanceSheetBinding;
import com.example.csedcw.ui.budget.BudgetViewModel;
import com.example.csedcw.ui.formatting.Formatters;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link BalanceSheet#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BalanceSheet extends Fragment {

    private BudgetViewModel budgetViewModel;

    private FragmentBalanceSheetBinding binding;

    private BalanceSheetTFAdapter balanceSheetTFAdapter;

    public BalanceSheet() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment BalanceSheet.
     */
    static BalanceSheet newInstance() {
        BalanceSheet fragment = new BalanceSheet();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.balence_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        NewCategoryFragment newCategoryFragment = NewCategoryFragment.newInstance("", 0, "Week");
        newCategoryFragment.show(requireActivity().getSupportFragmentManager(), "fragment_add_manager");

        return true;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        binding = FragmentBalanceSheetBinding.inflate(inflater);
        View view = binding.getRoot();

        budgetViewModel = new ViewModelProvider(requireActivity()).get(BudgetViewModel.class);

        RecyclerView recyclerView = binding.mainRecyclerView;
        recyclerView.setAdapter(
                balanceSheetTFAdapter = new BalanceSheetTFAdapter(getContext(), budgetViewModel, this));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);

        ((EditText) binding.totalBudgetText).setOnFocusChangeListener((v, isFocus) -> {
            if(!isFocus) {
                EditText editText = (EditText) v;

                int result = Formatters.parseMoneyString(editText.getText().toString());

                if (result != -1) {
                    if (result != budgetViewModel.getBudget()) {
                        Log.d(Constants.LOG_TAG, String.format("result = %d", result));
                        budgetViewModel.setBudget(result);
                        update();
                    }
                } else {
                    binding.totalBudgetText.setText(Formatters.moneyFormatter(budgetViewModel.getBudget()));
                    Toast toast = Toast.makeText(getContext(), "Invalid input.", Toast.LENGTH_LONG);
                    toast.show();
                }
            }
        });

        budgetViewModel.getCategoriesData().observe(getViewLifecycleOwner(), item  -> {
            recyclerView.setAdapter(
                    balanceSheetTFAdapter = new BalanceSheetTFAdapter(getContext(), budgetViewModel, this));
        });

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("Balance", "Resuming");
        update();
    }

    public void update() {

        Log.d("Balance", "Updated");

        balanceSheetTFAdapter.notifyDataSetChanged();

        for(int i = 0; i < binding.mainRecyclerView.getChildCount(); i++) {
            Log.d("Balance", "Updated child");
            ((RecyclerView) binding.mainRecyclerView.getChildAt(i).findViewById(R.id.categories_recycler)).getAdapter().notifyDataSetChanged();
        }

        binding.totalBudgetText.setText(Formatters.moneyFormatter(budgetViewModel.getBudget()));

        binding.savingsMoneyText.setText(Formatters.moneyFormatter(budgetViewModel.getBudget() - budgetViewModel.getTotalBudget()));
        binding.savingsPercentageText.setText(Formatters.percentageFormatter(((long) (budgetViewModel.getBudget() - budgetViewModel.getTotalBudget())) * 10000 / budgetViewModel.getBudget()));
    }
}
