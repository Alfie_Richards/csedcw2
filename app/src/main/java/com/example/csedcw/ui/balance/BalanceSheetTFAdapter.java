package com.example.csedcw.ui.balance;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.csedcw.Constants;
import com.example.csedcw.R;
import com.example.csedcw.model.transactions.TimePeriod;
import com.example.csedcw.ui.budget.BudgetViewModel;
import com.example.csedcw.ui.formatting.Formatters;

import org.jetbrains.annotations.NotNull;

import java.util.concurrent.atomic.AtomicInteger;

public class BalanceSheetTFAdapter extends RecyclerView.Adapter<BalanceSheetTFAdapter.MyViewHolder>{

    private Context context;
    BalanceSheet balanceSheet;
    private BudgetViewModel budgetViewModel;

    BalanceSheetTFAdapter(Context context, BudgetViewModel budgetViewModel, BalanceSheet balanceSheet) {
        this.context = context;
        this.balanceSheet = balanceSheet;
        this.budgetViewModel = budgetViewModel;
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView timeName, timeMoney, timePercentage;

        RecyclerView recyclerView;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);

            timeName = (TextView) itemView.findViewById(R.id.time_frame_name);
            timeMoney = (TextView) itemView.findViewById(R.id.time_frame_money_text);
            timePercentage = (TextView) itemView.findViewById(R.id.percentageText);

            recyclerView = (RecyclerView) itemView.findViewById(R.id.categories_recycler);
        }
    }

    @NotNull
    @Override
    public BalanceSheetTFAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                                 int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.timeframe_balance_section, parent, false);
        BalanceSheetTFAdapter.MyViewHolder viewHolder = new BalanceSheetTFAdapter.MyViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        TimePeriod timePeriod = budgetViewModel.getTimePeriod(position);

        if(timePeriod == null || timePeriod.getName() == null) {
            Log.d(Constants.LOG_TAG, "Null found time period.");
        }

        if(holder.timeName == null) {
            Log.d(Constants.LOG_TAG, "Null timeName found.");
        } else {
            holder.timeName.setText(timePeriod.getName());
            AtomicInteger totalBudget = new AtomicInteger();

            budgetViewModel.getCategoriesData().getValue().forEach(category -> {
                if (category.getTimePeriodName().equals(timePeriod.getName())) {
                    totalBudget.addAndGet(category.getTotalBudgetPence());
            }});

            holder.timeMoney.setText(Formatters.moneyFormatter(totalBudget.get() * timePeriod.getNumPerYear()));
            holder.timePercentage.setText(Formatters.percentageFormatter((int) (((long) 10000) * totalBudget.get() * timePeriod.getNumPerYear() / budgetViewModel.getBudget())));

            BalanceSheetCatAdapter catAdapter = new BalanceSheetCatAdapter(timePeriod, budgetViewModel, this);

            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
            holder.recyclerView.setLayoutManager(layoutManager);
            holder.recyclerView.setAdapter(catAdapter);
        }
    }

    @Override
    public int getItemCount() {
        Log.d("Balance", "Size Time Frames: " + budgetViewModel.getTimePeriodData().getValue().size());
        return budgetViewModel.getTimePeriodData().getValue().size();
    }
}
