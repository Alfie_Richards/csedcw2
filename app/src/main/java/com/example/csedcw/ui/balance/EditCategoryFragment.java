package com.example.csedcw.ui.balance;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.csedcw.R;
import com.example.csedcw.databinding.FragmentEditCategoryFragmentBinding;
import com.example.csedcw.ui.budget.BudgetViewModel;
import com.example.csedcw.ui.formatting.Formatters;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link EditCategoryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EditCategoryFragment extends DialogFragment {
    private static final String ARG_PARAM1 = "name";
    private static final String ARG_PARAM2 = "budget";
    private static final String ARG_PARAM3 = "time frame";

    private String mName;
    private int mBudget;
    private String mTimeFrame;

    public EditCategoryFragment() {
        // Required empty public constructor
    }

    public static EditCategoryFragment newInstance(String name, int budget, String timeFrame) {
        EditCategoryFragment fragment = new EditCategoryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, name);
        args.putInt(ARG_PARAM2, budget);
        args.putString(ARG_PARAM3, timeFrame);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mName = getArguments().getString(ARG_PARAM1);
            mBudget = getArguments().getInt(ARG_PARAM2);
            mTimeFrame = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentEditCategoryFragmentBinding binding = FragmentEditCategoryFragmentBinding.inflate(getLayoutInflater());

        binding.categoryNameTextView.setText(mName);
        binding.editCategoryBudgetEditText.setText(Formatters.moneyFormatter(mBudget));

        List<String> timeFrames = new ArrayList<>();
        (new ViewModelProvider(requireActivity()).get(BudgetViewModel.class)).getTimePeriodData().getValue().forEach(timeFrame -> {
            timeFrames.add(timeFrame.getName());
        });

        binding.editCategoryTimeframeSpinner.setAdapter(new ArrayAdapter(getContext(), R.layout.support_simple_spinner_dropdown_item, timeFrames));

        binding.editCategoryDeleteButton.setOnClickListener(view -> {
            this.dismiss();
            BudgetViewModel budgetViewModel = new ViewModelProvider(requireActivity()).get(BudgetViewModel.class);
            budgetViewModel.deleteCategory(mName);
        });

        binding.editCategoryDoneButton.setOnClickListener(view -> {

            String timeFrame = (String) binding.editCategoryTimeframeSpinner.getSelectedItem();
            int budget = Formatters.parseMoneyString(binding.editCategoryBudgetEditText.getText().toString());

            if(!mName.isEmpty() && !timeFrame.isEmpty() && budget > 0) {
                BudgetViewModel budgetViewModel = new ViewModelProvider(requireActivity()).get(BudgetViewModel.class);
                budgetViewModel.addCategory(mName, budget, timeFrame);
                this.dismiss();
            } else if(timeFrame.isEmpty()) {
                Toast.makeText(getContext(), "Invalid time frame", Toast.LENGTH_LONG).show();
            } else if(budget <= 0) {
                Toast.makeText(getContext(), "Invalid budget", Toast.LENGTH_LONG).show();
            }
        });


        return binding.getRoot();
    }
}
