package com.example.csedcw;

import com.example.csedcw.ui.formatting.Formatters;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

public class FormattingTest {

    @Test
    public void currencyFormatter_GivenPositiveAmount_FormatsCorrectly() {
        Assert.assertEquals("£12.92", Formatters.moneyFormatter(1292));
    }

    @Test
    public void currencyFormatter_GivenNegativeAmount_FormatsCorrectly() {
        Assert.assertEquals("-£4.70", Formatters.moneyFormatter(-470));
    }

    @Test
    public void currencyFormatter_GivenPositiveAmountString_ParsesCorrectly() {
        Assert.assertEquals(8523, Formatters.parseMoneyString("£85.23"));
    }

    @Test
    public void currencyFormatter_GivenNegativeAmountString_ParsesCorrectly() {
        Assert.assertEquals(10937, Formatters.parseMoneyString("-£109.37"));
    }
}
