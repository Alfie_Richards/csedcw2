package com.example.csedcw;

import com.example.csedcw.model.transactions.TimePeriod;

import org.junit.Assert;
import org.junit.Test;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Period;

public class TimePeriodTest {

    private static final TimePeriod week = new TimePeriod("Week", d -> d.getDayOfWeek().equals(DayOfWeek.MONDAY), 52);
    private static final TimePeriod month = new TimePeriod("Month", d -> d.getDayOfMonth() == 1, 12);
    private static final TimePeriod year = new TimePeriod("Year", d -> d.getDayOfYear() == 1, 1);

    @Test
    public void weekTimePeriod_EndDate_ShouldBeBeforeOneWeekAhead() {
        Assert.assertTrue(week.getEndDate().atTime(0, 0, 0).isBefore(LocalDate.now().plus(Period.ofWeeks(1)).atTime(23, 59, 59)));
    }

    @Test
    public void weekTimePeriod_StartDate_ShouldBeAfterOneWeekBefore() {
        Assert.assertTrue(week.getStartDate().atTime(23, 59, 59).isAfter(LocalDate.now().atTime(0, 0, 0).minus(Period.ofWeeks(1))));
    }

    @Test
    public void monthTimePeriod_EndDate_ShouldBeBeforeOneMonthAhead() {
        Assert.assertTrue(month.getEndDate().atTime(0, 0, 0).isBefore(LocalDate.now().plus(Period.ofMonths(1)).atTime(23, 59, 59)));
    }

    @Test
    public void monthTimePeriod_StartDate_ShouldBeAfterOneMonthBefore() {
        Assert.assertTrue(month.getStartDate().atTime(23, 59, 59).isAfter(LocalDate.now().atTime(0, 0, 0).minus(Period.ofMonths(1))));
    }

    @Test
    public void yearTimePeriod_EndDate_ShouldBeBeforeOneYearAhead() {
        Assert.assertTrue(year.getEndDate().atTime(0, 0, 0).isBefore(LocalDate.now().plus(Period.ofYears(1)).atTime(23, 59, 59)));
    }

    @Test
    public void yearTimePeriod_StartDate_ShouldBeAfterOneYearBefore() {
        Assert.assertTrue(year.getStartDate().atTime(23, 59, 59).isAfter(LocalDate.now().atTime(0, 0, 0).minus(Period.ofYears(1))));
    }
}
