package com.example.csedcw;

import com.example.csedcw.sorting.QuickSorter;

import junit.framework.JUnit4TestAdapter;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@RunWith(Parameterized.class)
public class QuickSortTest {

    private QuickSorter<String> quickSorter;
    private List<String> items;
    private List<String> expectedSortedItems;


    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {Arrays.asList("a", "b", "c"), Arrays.asList("a", "b", "c")},
                {Arrays.asList("c", "b", "a"), Arrays.asList("a", "b", "c")},
                {Arrays.asList("c", "a", "b"), Arrays.asList("a", "b", "c")},
                {Arrays.asList("qweuqwe", "jasdoa", "ancanscoi", "apsoiqwe", "mvncvx"), Arrays.asList("ancanscoi", "apsoiqwe", "jasdoa", "mvncvx", "qweuqwe")},
                {Arrays.asList("ac", "ab", "aa"), Arrays.asList("aa", "ab", "ac")}});
    }

    public QuickSortTest(List<String> items, List<String> expectedSortedItems) {
        this.quickSorter = new QuickSorter<>(String::compareTo);
        this.items = items;
        this.expectedSortedItems = expectedSortedItems;
    }

    @Test
    public void quickSorter_GivenUnsortedList_ReturnsSortedList() {
        quickSorter.quicksort(items, 0, items.size() - 1);

        Assert.assertArrayEquals(items.toArray(), expectedSortedItems.toArray());
    }
}
